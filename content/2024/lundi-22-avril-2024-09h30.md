+++
Author = "Lewo"
title = "Lundi 22 Avril 2024 09h30"
date = 2024-04-22T09:30:11+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
La nuit là, d’où j’écris, dans un abîme, j’entends miauler des violons. Mes sanglots longs rien qu’à moi. Un drôle de langage d’écorché où la vie se ratatine. C’est comme si j’avais déjà quitté mon enveloppe terrestre. Je ne mange plus, je ne dors plus, j’écris dans la brume. Vivant à grande peine dans une vapeur, à agiter des pensées de fantôme. Hiboux hululant au creux des catacombes. L’âme pas apaisée. Juste des dragées de toutes les couleurs pour pisser sur l’incendie. Drôles de sirènes. Et des aiguilles ici et là, qui pompent le peu qu’il reste dans les veines. Ils font qu’un tas de tout mon corps. Ils me recollent des morceaux de viandes au hasard. A l’intuition. Et ma poésie alors ?! Ah ! ce qu’il faut traverser de déluges pour souffler un peu ! Respirer ! Je réussirai !!!

