+++
Author = "Lewo"
title = "Mercredi 21 Août 2024 19h46"
date = 2024-08-21T19:46:32+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Je ne sais plus être amoureux, je ne sais plus être serein, léger. Je fracasse ma vie, mes sentiments. Je voulais me venger de moi-même. De cette pute de maladie qui me bouffe. Au final, je suis le pyromane de mon âme. J’ai perdu des bouts magnifiques de vie. J’ai fui la ronde des sentiments. Un con, un véritable con !
