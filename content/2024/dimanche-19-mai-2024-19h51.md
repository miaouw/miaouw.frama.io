+++
Author = "Lewo"
title = "Dimanche 19 Mai 2024 19h51"
date = 2024-05-19T19:51:35+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Autant les plaisirs sont brefs, autant les ennuis finissent jamais… Ils se nouent les uns dans les autres, c’est un vrai enchevêtrement. Y’en a bien trop pour ma force… Je me recroqueville dans le malheur, me décompose, me mutile au désespoir. Je me morfonds férocement pour opposer moins de surface… Même me faufiler par-dessous les catastrophes… Rien à faire ! Je me fais cueillir quand même. Les ennuis, je n’existe que par eux.
