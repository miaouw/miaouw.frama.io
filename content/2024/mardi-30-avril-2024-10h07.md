+++
Author = "Lewo"
title = "Mardi 30 Avril 2024 10h07"
date = 2024-04-30T10:07:19+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
J’ai le sentiment que de la vie il m’en reste encore beaucoup en dedans. Qu’elle se défend pour ainsi dire. Ce sont des petits riens, comme ça, en passant, ça me dit quelque chose. Des émotions, des sentiments, de petites fièvres intérieur… C’est presque ça la vie, des riens dont on peut faire un monde. Essayer une grande respiration, au hasard des coups de vents. Une curiosité, un petit art, un souffle… 
