+++
Author = "Lewo"
title = "Samedi 20 Avril 2024 09h30"
date = 2024-04-20T09:30:13+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++

Quelquefois j’avoue, le chagrin me poigne. Tout m’attrape. L’âme maudite. Que les nuits sont des enfers infinis. Que je pourrais m’emboutir la tête. Faire trembler les murs… de tout cœur !  

Mais ce matin j’ai vu du printemps. Un printemps qu’on attend pas. Deux petites mésanges au bord de la fenêtre venue picorer les quelques miettes de pain que j’avais laissé pour elles. Ça piaille, ça fait des boules de couleurs qui semblent laver la nuit et faire fleurir la lumière. C’est une danse avec des courbes fragiles et agiles...  
C’est con comme toute chose, c’est beau et ça donne à penser. 
