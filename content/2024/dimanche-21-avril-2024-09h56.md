+++
Author = "Lewo"
title = "Dimanche 21 Avril 2024 09h56"
date = 2024-04-21T09:56:42+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
L’hôpital. Le froid. Ce genre d’endroit, quand on y reste un peu longtemps, on y reste toujours un peu. Ça fascine, ça obsède, ça possède en sorte. Avec toujours la trouille d’y revenir un jour à l’autre… La trouille des grilles, des hauts murs, tout à doubles tours. La vie ça ne sert qu’à vivre. On voudrait la retenir, mais on y arrive pas. Ce n’est pas simple comme voyage. Cascades de catastrophes. On se cherche une luciole, une aurore au cœur de la nuit...

