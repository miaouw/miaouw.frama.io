+++
Author = "Lewo"
title = "Lundi 29 Avril 2024 17h50"
date = 2024-04-29T17:50:50+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
C’est une fatigue qui n’a pas du nom, celle qui tient de l’angoisse. L’âme trébuche tout à coup et viennent aux yeux les larmes. Rien qui délivre, qui apaise, qui allège, plus rien qui ne fasse danser… D’un seul instant l’avenir devient du pire en rond. On songe seulement à soi-même, au chaos qui nous mène, à l’erreur de se rencontrer… de rentrer dans le désordre pour toujours ! Comme l’errance d’un écho. C’est toute une humanité en raccourci ! Il y a des malices dans ces tréfonds. Des sortilèges. Parfois on croit tomber dans un moment de grand miracle. On se dit qu’on a sa poésie quand-même, du moment qu’on vit encore… La vie se contredit tellement ! J’ai tous les vertiges d’un bateau au creux de mon âme !
