+++
Author = "Lewo"
title = "Mercredi 28 Août 2024 17h48"
date = 2024-08-28T17:48:45+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
J’ai mal au corps. J’ai mal à l’âme. Je voudrais me vomir une fois pour toutes ! Être capable de s’imposer à soi. Être vertical. Avoir le temps de devenir un meilleur humain. Je vis l’exil permanent, avec un forte attirance furieuse pour la disparition. Je voudrais ne pas penser à mon chagrin, seulement à celui que je fais ! Savoir lire la vie c’est un paysage qui recommence, à condition de savoir déchiffrer les signes. Dans chaque bonhomme y a un secret !
