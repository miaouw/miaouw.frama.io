+++
Author = "Lewo"
title = "Dimanche 28 Avril 2024 08h56"
date = 2024-04-28T08:56:43+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Je voulais me promener un peu… il pleut ! La nature est espiègle, taquine et narquoise ! Elle doit m’en vouloir pour quelque-chose, alors elle chatouille… boude un peu, renâcle. Et moi me voilà tout puzzle ! Je tourne en rond… enfin je crois que ce sont des ronds ?! Je ne sais plus me retrouver. Ni mort, ni vif, ni quoi… L’hôpital est un lieu étrange, on en prend l’hébétude !
