+++
Author = "Lewo"
title = "Mercredi 24 Avril 2024 17h19"
date = 2024-04-24T17:20:00+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
La dépression, l’ennemi mortel du rêve ! Ça vous sabote le vivant.  
Ça s’installe jusqu’à devenir une partie de soi, sans qu’on s’y habitue jamais ! Ça rayonne. Toujours aussi neuve, toujours aussi sauvage. Ça ne veut pas franchement dire son nom, ça se cache dans les plis et les replis de l’esprit et du corps.  

Pourtant je cherche, je fouille, je touille les débris. Tout entier dans l’incendie à chercher une clef. Rien à faire ! Pleurer beaucoup et pour rien. Tout en noir et blanc. Ça donne une danse dans les tons gris sales. Et muette. Il faudrait éteindre ce silence. J’éponge la vie au fur et à mesure qu’elle s’écoule. Parfois il y a des arcs-en-ciel de mieux. Mais il y a des jours ou elle ne demande qu’à s’évader, la vie. Ça me dépasse… vivre à regret, à tout petits coups... 
