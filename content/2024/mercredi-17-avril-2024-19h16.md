+++
Author = "Lewo"
title = "Mercredi 17 Avril 2024 19h16"
date = 2024-04-17T19:16:54+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Il faut se le dire, j’ai bien du mal à penser à autre chose qu’à mon destin en sursis. Il y a des jours difficiles ou je ne sais plus si je parle ou si je pleure. Je suis comme un vieux môme, quelque-chose c’est refermé derrière moi et c’est comme si je n’avais pas eu le temps de prendre mes affaires de grande personne. Ca donne des choses lisses, tristes, muettes, désarmés et ça me fait douter des anges...
