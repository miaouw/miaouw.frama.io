+++
Author = "Lewo"
title = "Jeudi 09 Mai 2024 09h41"
date = 2024-05-09T09:41:12+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Ce salaud de printemps m'a réveillé une belle maladie ! Je ne suis plus assez, avec moi seul, j'en ai marre de moi, j'en ai fait le tour… Il va furieusement falloir que j’aille faire un tour du côté du bonheur… connais pas le chemin !

Commencer par faire une brèche dans ce mur que j’ai bâti entre moi et mon âme. Remonter le fil, à petits pas, à petits mots. Ne rien presser pour ne rien casser… Trouver un langage. Il existe de ces moments de désolation où l’esprit ne semble déjà plus avec le corps. Il s’y trouve trop mal, vraiment, acculé. C’est déjà presque plus qu’un morceau de fantôme qui parle. Un morceau d’illusion. Je bute dans des riens. Je rêve trop. Je glisse sur tous les mots. Je m’émiette. Trouver les mots, solides, fendre la muraille qui cache je ne sais quelle obscurité… Je suppose qu’il faut bien passer par là pour entrer enfin dans le fond de la vie.
