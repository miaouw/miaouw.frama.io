+++
Author = "Lewo"
title = "Mercredi 01 Mai 2024 20h16"
date = 2024-05-01T20:16:37+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Sa platitude à soi, sans horizon, sans une seule montagne, sans un putain d'océan… juste un abîme malade, exténué du vivant ! L’humanité qui se perd dans des replis lointains. Si vague, si étrangers, si écarté qu’on ne peut plus penser à rien. Avec seulement du silence pour combler les secondes… Et pourtant, s’acharner à croire qu’on a encore des nébuleuses à portée de la main…
