+++
Author = "Lewo"
title = "Vendredi 23 Août 2024 19h20"
date = 2024-08-23T19:20:42+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
C’est une fatigue qui n’a pas de nom, celle qui tient de l’angoisse. Sans doute qu’il faudrait dormir, redevenir un homme comme un autre. Trop fatigué pour avoir même l’élan de se tuer. Tout est fatigue. Je ne sais pas quoi penser moi. Pas en état de réfléchir trop fort. Ca me tracasse salement tout de même, l’horreur où je me trouve, en plus du bruit de tempête que je promène… Je sens que de la vie il en reste encore beaucoup en dedans. Elle se défend. Ca brille pas fort l’espérance, une maigre lumière au fin bout d’un infini corridor hostile. Il y a les échos d’une fanfare particulière dans la tête. C’est violent, mais c’est énorme la vie quand-même. On se perd partout.  

Et la mémoire, faut se méfier ! C’est méchant le passé. Un peu putain. Ca fond dans une rêvasserie. Ca prend de petites mélodies qu’on lui demandait pas. Ca revient tout maquillé de pleurs et de remords. C’est pas sérieux ! Il faudrait que je trouve un petit quelque-chose bien délirant pour compenser tout le chagrin d’être pour toujours enfermé dans ma tête…
