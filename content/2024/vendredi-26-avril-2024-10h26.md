+++
Author = "Lewo"
title = "Vendredi 26 Avril 2024 10h26"
date = 2024-04-26T10:26:40+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Je ne pense pas qu’on puisse prévoir l’avenir, mais peut-être faut-il au moins se le permettre ?! C’est fou ce qu’il faut s’inventer de réalités autour de soi. Des réalités qui puissent durer, se maintenir nus et toutes entières. La vie se contredit tellement ! Il faut de la débrouille, de l’astuce, des échappatoires. Oublier que hier n’existe plus, c’est juste une photo, une larme. Que demain ce n’est que la mort. Simple et con comme le temps qui passe. Reste encore les rêves qui devraient tout entiers dévorer la vie, pas l’inverse…  
Il existe probablement, à la sortie de cet hôpital, un inconnu qui m’attend. Moi...
