+++
Author = "Lewo"
title = "Mardi 23 Avril 2024 20h38"
date = 2024-04-23T20:38:24+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Promenade dans les rues. Tout est du bruit. Tout ! que même l’écho n’arrive pas à suivre. Je me dissimule en rasant les murs, dans l’ombre. Quelques heures à marcher dans le blafard. A nager entre l’asphalte et le pavé. A contre-courant. Tu parles d’une promenade ! Une course vers un néant profond. Même noyé dans la foule, j’ai la tête basse et la marée me monte aux yeux ! Moi qui n’avais peur de rien. J’ai le doute coupé en deux… avec une désespérante envie de regarder l’incroyable. Mais ce vertige… cette saloperie ! Retour au panier. Pas beaucoup plus serein en vérité. Mais il faut bien être quelque-part... 
