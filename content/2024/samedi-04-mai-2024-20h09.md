+++
Author = "Lewo"
title = "Samedi 04 Mai 2024 20h09"
date = 2024-05-04T20:09:42+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Il y a des jours ou des bouts de santé foutent le camp. Les nuits sont longues, insensées. Les petits matins terribles. Les journées infinies, cruelles, démesurées et féroces. Dès que la tête se met à piétiner, à se chercher des raisons sérieuses de vivre… je m’enfonce dans de mauvais fiels. Il y a des jours, je crois, que j’étais quelqu’un, quand même malgré tout, aujourd’hui, hier, demain, toujours… où suis-je ?!
