+++
Author = "Lewo"
title = "Lundi 19 Août 2024 16h21"
date = 2024-08-19T16:21:39+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Des choses, des choses encore, des gens autour, des courants d’air lourd de terreurs innombrables. Le tout baigné dans des formes imprécises, croisées, menaçantes, fondues, mêlées à des passés incertains… Autour, le réel, le banal et tout ce qui vient s’ajouter encore à l’absurde par un maléfice de mon esprit sans limites. Si seulement il me restait dans les veines le moindre soupçon d’instinct… j’irai lui hurler que je l’aime !
