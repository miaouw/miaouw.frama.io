+++
Author = "Lewo"
title = "Mercredi 29 Mai 2024 17h41"
date = 2024-05-29T17:41:17+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Je suis à la traîne du reste du monde… à la traîne… Je me raccroche autant que je peux tout au rebord du noir. Mais je m’affale, m’accroche, dévisse, me rattrape de justesse, etc... Plus de force du tout ! Je voudrais courir. Il me faut du vent… ! De la fête… ! Des fanfares ! Plein de tonnerres ! Des paysages ! Des couleurs ! Des féeries… ! Hélas, tout cède, tout flanche, casse un moment. Tombe et retombe…
