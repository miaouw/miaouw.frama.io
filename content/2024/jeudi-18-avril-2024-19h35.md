+++
Author = "Lewo"
title = "Jeudi 18 Avril 2024 19h35"
date = 2024-04-18T19:35:33+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Quand ça se met à pleuvoir les catastrophes, il en est d’inattendues qui vous déchirent. Du ciel intime, on peut s’attendre à tout dans les moments difficiles. Il s’acharne on dirait. Il vous veut pantelant. C’est tout de même un spectacle ! Un vacarme ! C’est tout des mystères qui attendent. Une pâleur, une transparence que le monde tout entier pense voir à travers. Connerie ! Il y a des coins ou la vie se ratatine sans laisser de couleur. Je me croyais plus solide que je n’étais. En fait, ça fait des années que je patauge en pleine déroute. Jamais bon trapéziste. En pleine déroute. Arrive un moment ou ce n’est plus possible de trimballer impunément tout un cimetière sur son dos…
