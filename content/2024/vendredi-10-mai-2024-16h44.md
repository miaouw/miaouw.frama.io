+++
Author = "Lewo"
title = "Vendredi 10 Mai 2024 16h44"
date = 2024-05-10T16:44:50+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Il y a de grands moments affligeants où je me fous d’être heureux ! Mon chantier d’humain, les mille besognes de mon âme et l’infini rafistolage de mon esprit me prennent tout mon temps. Jours désenchantés où tout se débine, sans savoir que faire pour être rayonnant. Jours amers. Jours solitaires où tout se refuse et rien ne sert !
