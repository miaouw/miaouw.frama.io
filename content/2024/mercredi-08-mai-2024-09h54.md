+++
Author = "Lewo"
title = "Mercredi 08 Mai 2024 09h54"
date = 2024-05-08T09:54:35+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
C’est difficile d’être entouré d’autant de monde et d’être pourtant si seul, tellement seul ! Que même mon ombre semble me suivre à regret… Petit à petit je me recroqueville dans ma plaie, dans mon silence. Tout absent de ma vie. Je la regarde sans y toucher. Toujours de loin, jamais dedans. Seul avec moi-même, étranger à tout. Il va falloir que je trouve une astuce pour aller voir du côté du bonheur. Le bonheur, ce tout petit truc de rien qui fiche le camp dès que tu tournes le dos.  
Commencer peut-être par tenter un petit pas vers l’autre ?!
