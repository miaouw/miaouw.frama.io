+++
Author = "Lewo"
title = "Dimanche 12 Mai 2024 10h42"
date = 2024-05-12T10:42:47+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Mauvaise journée. Je passe des heures, couché à ne rien faire. A attendre une pensée meilleure. Un semblant de lumière. L’impression d’être au fond d’un gouffre.  

Je suis resté des années sans boire. Des années à me protéger du suicide, de la peur, de la grisaille. J’ai perdu des traces entières de ma vie. A ne plus rien vouloir. A errer dans un long couloir. Aucune sortie. Aucune porte. Plus aucun courage. Plus de force. L’impression de doucement m’enfoncer dans des sables mouvants. Jours après jours, le petit quotidien sans fond à remplir le puits du temps qui passe avec du rien. Comme si la vie avait tout donné. Terribles moments où rien ne me sauve, ne me sauvera jamais… C’est si lourd, chaque matin. Porter par petit paquet ce qui brûle l’âme.
