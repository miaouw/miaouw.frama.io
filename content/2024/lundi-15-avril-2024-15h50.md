+++
Author = "Lewo"
title = "Lundi 15 Avril 2024 15h50"
date = 2024-04-15T15:50:00+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
Un humain c’est vachement autre chose qu’un petit tas de secrets. Bien plus qu’autre chose ! Comme un pays sans frontières, l’horizon qui ne tient qu’aux yeux. C’est un lieu rêvé quand on ne rêve pas encore. Un rêve qui vous mène quand tout dort, quand on est soi-même en songe. Au réveil, ça vous colle à la peau. Tout entier. Ça vous remplit et vous vide tour à tour. Entre l’étale et le jusant. La plénitude et le manque. Comme l’océan, ça file d’un bord à l’autre de lui-même. Un pays qui marche, boiteux parfois, cassé souvent, tanguant mais debout. En avant, levé, dressé à demi, chancelant. Même s’il ne reste qu’à peine le reflet de son ombre : en avant calme et fou !
