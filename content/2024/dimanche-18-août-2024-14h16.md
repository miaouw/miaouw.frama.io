+++
Author = "Lewo"
title = "Dimanche 18 Août 2024 14h16"
date = 2024-08-18T14:16:31+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++

Dès que dans l’existence ça va un tout petit peu mieux, on se fait rattraper ! Ca vient sans prévenir, c’est violent, terrible. Alors la désillusion est trop forte ! On supporte plus son chagrin, celui qui est toujours là, dans  l’ombre. C’est pas supportable à regarder en face, c’est pas respirable. C’est dégueulasse de me retrouver encore ici, dans l’interminable ennui de cet hôpital, avec juste l’envie d’en finir. Une bonne fois pour toutes ! Dès que dans l’existence ça va un tout petit mieux, qu’on croit...
