+++
Author = "Lewo"
title = "Dimanche 05 Mai 2024 20h25"
date = 2024-05-05T20:25:33+02:00
Categories = ["2024"]
menu = "main"
draft = false
art = false
+++
La solitude absolue de la pure solitude… Ça frôle la colère parfois ! Je voudrais la paix et le silence de ma colère ! Fatigué de ces inutiles détours par les champs carbonisés de ma mémoire. Je suis tout entier plus éparpillé qu’un feu d’artifice… 
