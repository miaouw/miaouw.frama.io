+++
Author = "Lewo"
title = "Samedi 02 décembre 2017 15h52"
date = 2017-12-02T15:52:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

Lundi, le bouquin s’en va à l’imprimerie...<br />
Six ans après, mon vieux, c’est pas domage !<br />
Trois cent trente-six pages. Édition limitée pour correction, après, après...

Le deuxième volume lui, il attend son heure. Il est déjà là. Il surveille. Il veut juste voir la gueule du grand frère avant de bondir à son tour.

