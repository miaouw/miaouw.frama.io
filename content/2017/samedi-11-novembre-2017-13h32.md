+++
Author = "Lewo"
title = "Samedi 11 novembre 2017 13h32"
date = 2017-11-11T13:32:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

Note :<br />
Changement du moteur de miAouw. Retour au  statique.<br />
C'est moins pratique (pas de pagination) mais bien plus rapide
et confortable pour la rédaction... enfin, si je me remet à écrire un jour ! 

Pensez à mettre à jour vos flux RSS.


