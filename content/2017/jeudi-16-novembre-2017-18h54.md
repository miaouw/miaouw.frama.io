+++
Author = "Lewo"
title = "Jeudi 16 novembre 2017 18h54"
date = 2017-11-16T18:54:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

Je bricole. Je boutique. Je trafique. Je magouille. Je maquignone...<br />
De la fumée, du fard. Souvent je bidouille.<br /> 
Même, je déguise. Je m'accorde. Enfin... c'est artifices et boniments. Fables et déguisements... je tronque !
J'ampute ! Je dérive, j'élague, je bifurque, j'altère... je galope quoi !<br /> 
D'accord ! Et puis ?

Heu... pirate ?! Roublard, routier, filou, pillard... et fine mouche même !!!<br />
Et ???<br />
Et... ?! Et rien de plus !  Jamais, jamais de mensonge !<br />
Magicien en somme !

