+++
Author = "Lewo"
title = "Samedi 15 juin 2017 03h15"
date = 2017-06-15T03:15:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

Nos doutes, nos rêves, leurs mots fumeux,<br />
Nos sourires au-dessus des frontières,<br />
Tous ceux qui voulais nous tailler comme eux,<br />
Qui de nous à eux, vieillira le mieux ?!

