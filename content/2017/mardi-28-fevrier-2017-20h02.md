+++
Author = "Lewo"
title = "Mardi 28 février 2017 20h02"
date = 2017-02-28T20:02:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

Je viens des cîtés du sable. J'arrive dans le profond du béton... dans le coeur des villes bidons. J'habite un continent électronique. J'ai mes réflexes d'Europe qui pagaient dans mes rêves d'Afrique. Je viens de la pluie et de l'orage, je viens avec un équilibriste dans la tête, je viens avec... un rien de rage !!!
