+++
Author = "Lewo"
title = "Mardi 07 mars 2017 01h38"
date = 2017-03-07T01:38:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
+++

J'étais déjà loin. Dans des étincelles douloureuses. Dans l'obscurité.<br />
Tu n'étais pas encore là...



