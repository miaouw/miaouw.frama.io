+++
Author = "Lewo"
title = "Jeudi 21 septembre 2017 11h32"
date = 2017-09-21T11:32:23+01:00
Categories = ["2017"]
menu = "main"
draft = false
art = true
+++

<div class="center">
<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/img/novgorote_gd.jpg" title="La Légende de Novgorode - B. Cendrars" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/img/novgorote_min.jpg" alt="La Légende de Novgorode" /></a>
</div>
<p>Trouver l'introuvable...</p>
</div>
