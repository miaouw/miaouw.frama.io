+++
Author = "Lewo"
title = "Dimanche 12 novembre 2017 00h44"
date = 2017-09-12T00:44:23
Categories = ["2017"]
menu = "main"
draft = false
+++

Ton prénom dans les plis de mes draps.<br />
Mon cœur en sang,<br />
Tout  en désastre,<br />
Tout en larmes. <br />
Mon putain de cœur !<br />
Il me faudra le porter<br />
Comme un chien qui porte à son panier<br />
Sa patte cassée.<br />
Il me faudra les porter aussi<br />
Ton prénom de légende...<br />
Et puis tes yeux !


