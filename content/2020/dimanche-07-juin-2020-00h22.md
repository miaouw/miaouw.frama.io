+++
Author = "Lewo"
title = "Dimanche 07 Juin 2020 00h22"
date = 2020-06-07T00:22:28+02:00
Categories = ["2020"]
menu = "main"
draft = false
art = false
+++

Un verre de gloria.  
Trois calumets anciens.  
Tout se mélange...  
Les cigares, la vodka.  
Le rhum, les berceuses.  
Et puis quoi ?  
Les chants, les chamans.  
Les sentiers de feu, les mocassins.  
Les bracelets de verre.  
Les jungles barbares.  
La hache de guerre.  
Tout se mélange...  
Miel, maïs et sacrifices.  
Soleils et éclipses.  
Arcs, flèches et tomahawks.  
Terres et ressacs.  
Totems et glyphes.  
Griffes d’ours.  
Dents de puma.  
Lunes rousses.  
Plumes d’ara.  
Le plomb, l’or.  
Et encore...  
Les tasses de mezcal.  
L’aigle et le quetzal.  
Les rêves et la fumée.  
Enfin...  
Tout va se mêler... !

