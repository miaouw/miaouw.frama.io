+++
Author = "Lewo"
title = "Mercredi 24 Juin 2020 01h51"
date = 2020-06-24T01:51:04+02:00
Categories = ["2020"]
menu = "main"
draft = false
art = false
+++

Douze cordes...  
Du bois fin !  
Faut voir comme...  
Table en sapin.  
Manche en charme.  
Touche en mûrier.  
Corps en châtaignier.  
Rosace en pleksi.  
Chevalet en mûrier, itou.  
Sillet en buis.  
Chevilles en ébène.  
Douze cordes !  
Foutre !  
Une de trop...  
C'est rien !  
Voilà !  
Entre les mains,  
Une plume !  
Enfin !
