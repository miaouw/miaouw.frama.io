+++
Author = "Lewo"
title = "Samedi 11 Juillet 2020 01h48"
date = 2020-07-11T01:48:38+02:00
Categories = ["2020"]
menu = "main"
draft = false
art = false
+++
La petite pélopée, ce matin, juste là...  
Petite mutine qui fabrique son nid.  
Trois tours et puis va...  
Petite qui encourage à chausser les souliers.  
Le peu de souffle à trouver...  
La route, le doute...   
Encore, en avant toute !
