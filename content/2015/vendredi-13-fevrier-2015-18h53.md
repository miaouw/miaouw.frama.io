+++
Author = "Lewo"
title = "Vendredi 13 février 2015 18h53"
date = 2015-02-13T18:53:00
Categories = ["2015"]
menu = "main"
draft = false
+++

"Ce qu’il faut de nuit  
Au-dessus des arbres,  
Ce qu’il faut de fruits  
Aux tables de marbre,  
Ce qu’il faut d’obscur  
Pour que le sang batte,  
Ce qu’il faut de pur  
Au cœur écarlate,  
Ce qu’il faut de jour  
Sur la page blanche,  
Ce qu’il faut d’amour  
Au fond du silence.  
Et l’ame sans gloire  
Qui demande à boire,  
Le fil de nos jours  
Chaque jour plus mince,  
Et le cœur plus sourd  
Les ans qui le pincent.  
Nul n’entend que nous  
La poulie qui grince,  
Le seau est si lourd."  

<em>Jules Supervielle</em>
