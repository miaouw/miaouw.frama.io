+++
Author = "Lewo"
title = "Dimanche 03 Mars 2019 09h33"
date = 2019-03-03T09:33:17+01:00
Categories = ["2019"]
menu = "main"
draft = false
art = true
+++

<a href="https://download.tuxfamily.org/miaouw/statique/img/cheshire_gd.png" title="Parkinson gribouille un chat" class="zoombox">
<span class="zoomomie"><img src="https://download.tuxfamily.org/miaouw/statique/img/cheshire_min.png" alt="Parkinson dessine un chat" width="350" height="175" class="leftmo" /></span></a>J'ai perdu ma plume ! C'est dingue...  
Suffit de voir le trait tout pété de mes dessins ces derniers jours. Tout est gribouillage, tremblement, brouillon, crade, baclé... répugnant quoi !  

Il falloir s'y remettre, fermement !  
Ou bien abandonner, définitivement...  

<div style="clear:both;"></div>
