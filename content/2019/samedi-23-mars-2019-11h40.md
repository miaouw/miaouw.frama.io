+++
Author = "Lewo"
title = "Samedi 23 Mars 2019 11h40"
date = 2019-03-23T11:40:39+01:00
Categories = ["2019"]
menu = "main"
draft = false
art = false
+++

Jouir du moment présent. Ne rien attendre de demain.  
Se masturber frénétiquement face aux éclatantes manifestations du vivant… Apprécier tout, tout le temps. Reconnaître le bien-être dans le profond de ses plus humbles expressions. Être à l'affût de l'instant et savoir le nommer, le saisir, le chérir et tout le tremblement. Parce que tout passe, tout coule, tout s'écroule... 

Ce matin, la petite branche dans le vent. L'oiseau qui s'étire en chantant doucement. Le premier rayon de soleil posé juste là. L'odeur du pain chaud. La trace fragile d'un escargot massacré sur la route. La lente agonie du silence dans le cri déchirant des poivrots qui rentrent chez-eux. Mes entrechats entre les poubelles éventrées. L'aurore qui se mire dans des flaques de pisses. Les bagnoles de police qui rampent vers le prochain crime à commettre. Le fantôme d'un cadavre humain allongé sous un porche fleuri. Les regards indifférents qui promènent leur chien de faïence. Le délicat bal des mouches sur ce rat crevé et puis, et puis... merde ! Le stoïcisme !  

Je ne suis même pas certain d'avoir fait seulement cinq cents mètre…  

Ils me font chier les ayatollahs du carpe diem. Tous les apôtres de l’espérance qui "s'obstinent à voir le noir en blanc" (Ambrose Bierce). Je pense que la naïveté est une maladie d'enfance plutôt bonne à entretenir, avec modération. Que l'espoir est un cancer qui, heureusement, a ses rémissions...   

Mais par contre... Par contre, le prochain connard d'aveugle affable qui me cause vie en rose, bonheur partout, instant présent et surtout, putain, à tous les étages, optimisme : je le crame !!! 
