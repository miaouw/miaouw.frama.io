+++
Author = "Lewo"
title = "Jeudi 28 Février 2019 11h38"
date = 2019-02-28T11:38:24+01:00
Categories = ["2019"]
menu = "main"
draft = false
art = true
+++
<a href="https://download.tuxfamily.org/miaouw/statique/img/sparrow_gd.png" title="Oiseau-tempête" class="zoombox">
<span class="zoomomie"><img src="https://download.tuxfamily.org/miaouw/statique/img/sparrow_min.png" alt="Oiseau-tempête" width="300" height="127" class="rightmo" /></span></a>
Pour tuer le temps, je m'invente des oiseaux...<br />
Je m'en vais braconner dedans mes imaginations. Ce matin encore, j'ai vu un petit oiseau-tempête perché sur la toute dernière branche de la nuit. Il m'a donné une plume en bois et puis, il s'est envolé en craquant...  

<div style="clear:both;"></div>
