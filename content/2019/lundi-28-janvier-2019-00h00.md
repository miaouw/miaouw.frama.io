+++
Author = "Lewo"
title = "Lundi 28 Janvier 2019 00h00"
date = 2019-01-28T00:00:00
Categories = ["2019"]
menu = "main"
draft = false
+++

Plop !  
Changement de moteur, c'est maintenant [Hugo](https://gohugo.io/ "goHugo") qui fait tourner le zinzin...

-- Le minou reste en mode statique/HTML.  
-- La tapisserie reste la même que depuis ~2016.  
-- On garde le mode journal (titre = date+heure).  
-- Pagination en bas de page (enfin !).  
-- Encore quelques réglages à faire ici et là...  
-- <span style="text-decoration:line-through;" class="erase">A terme, pas mal de changements à prévoir...</span>  
-- Nouvelle année : les anciens articles sont descendus aux [archives](/archives).

