+++
Author = "Lewo"
title = "Mercredi 27 Février 2019 12h06"
date = 2019-02-27T12:06:00
Categories = ["2019"]
menu = "main"
art = true
draft = false
+++
<p class="center"><a href="https://download.tuxfamily.org/miaouw/statique/img/squetrim_gd.png" title="Capitaine gueule d'amour" class="zoombox">
<span class="zoomomie"><img src="https://download.tuxfamily.org/miaouw/statique/img/squetrim_min.png" width="400" height="378" alt="Capitaine gueule d'amour" /></span></a><br />
<span class="legend">Capitaine au long cours à bord d'un cerf-volant<br />
Buvant aux morts d'amour avec le coeur tressant<br />
Trois petits tours autour d'un mauvais noeud coulant.<br /></span></p>  





