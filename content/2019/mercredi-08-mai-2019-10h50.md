+++
Author = "Lewo"
title = "Mercredi 08 Mai 2019 10h50"
date = 2019-05-08T10:50:23+02:00
Categories = ["2019"]
menu = "main"
draft = false
art = false
+++

Le vieux capitaine est ici.  
On a posé au sec, en père peinard, Lullaby.  
Un bouquin est au chaud, à l'imprimerie.  
Lundi, c'est déjà le boulot.  
Ne pas y penser !  
Le chat dors comme une nature morte.  
La douce a le nez dans ses études.  
Je fume.  
Un cargo me traverse l'ennui.  
Le printemps se fait sa mise en pluie...
