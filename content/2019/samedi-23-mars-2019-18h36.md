+++
Author = "Lewo"
title = "Samedi 23 Mars 2019 18h36"
date = 2019-03-23T18:36:39+01:00
Categories = ["2019"]
menu = "main"
draft = false
art = false
+++

En écho à ma promenade matinale, j'ouvre un bouquin et je tombe sur cette petite maxime de Sébastien-Roch Nicolas de Chamfort :  

_"La meilleure philosophie, relativement au monde, est d’allier, à son égard, le sarcasme de la gaîté avec l’indulgence du mépris."_
