+++
Author = "Lewo"
title = "Vendredi 24 janvier 2014 01h45"
date = 2014-01-24T01:45:00
Categories = ["2014"]
menu = "main"
draft = false
+++
Pied de poule.  
Accommoder selon les goûts :

-- 10 cl de lait de vache  
-- 4 cl de whisky de n'importe-où  
-- 2 cl de sucre de canne  
-- 1 jaune d'œuf de poule  

Frapper fort. Boire violemment.  
Se répéter que la mélancolie, c'est la misère de l'âme qui fait voir le squelette de toutes choses... 
