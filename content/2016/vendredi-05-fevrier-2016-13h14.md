+++
Author = "Lewo"
title = "Vendredi 05 fevrier 2016 13h14"
date = 2016-02-05T13:14:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="center">
<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/tralala_gd.jpg" title="Au boulot !" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/tralala.jpg" alt="Au boulot !" /></a>
</div>
</div>
<p style="text-align:center">
Même pas mort ! C'est juste que bon... je manque de temps.<br />
J'habite au milieu des couleurs, dans une forêt de pinceaux. Faut bien gagner sa croûte. Essayer, au moins.<br />Plus de nouvelles bientôt...</p>
