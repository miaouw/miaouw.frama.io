+++
Author = "Lewo"
title = "Lundi 04 janvier 2016 14h45"
date = 2016-01-04T14:45:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Je voulais écrire. Tous les jours. Poser des mots. Asseoir les choses, garder des traces. Prendre du temps. Écrire. Dessiner. Éviter les photos faciles. Vraiment prendre du temps. Pour moi, pour la mémoire, pour le partage, pour… rien. Dix jours plus tard : pas un mot. Pas une ligne. Pas un trait. Pas même une photo. Rien !<br />
Enfin, c’est mentir un peu. J’ai vaguement gribouillé. Trois fois rien. Une petite bouteille à la mer pour un marin qui manque. Histoire de...

Quelques heures d’avion, une escale, encore quelques heures d’avion, et puis voilà… j’y suis !

Les évidences, les habitudes, le souffle, le courage. Tout déménage. Tout pète. Tout explose.<br />
Les premiers jours ont été durs. Ma cervelle a pris l’eau. Des fuites de partout. Incapable de tout. Capable de rien. Paralytique, chaos debout, presque aphone. Complètement perdu. Petit chat tremblant balancé dans la bassine d’eau glacée de la réalité. C’est la vie qui débarque. C’est la gueule qui s’en prend plein la gueule. Mon âme, c’est Varsovie après les massacres, c’est la douceur d’un soleil d’Austerlitz. C’est le bordel sans fin.

Trois ou quatre jours plus tard, la respiration revient un peu. Doucement. Je compose avec le puzzle qui me sert de cerveau. Je pose un peu. J’ordonne vaguement. À grands coups de pioches, je déterre des mots. J’énumère, je résume à des épithètes. C’est dur. Enivrant. Violent. Tendre. Brutal. Câlin. Lent. Agile. Soudain. Halluciné et hallucinant. Et les couleurs. Et les fragrances. Et ça rôde. Ça plane. Ça travail. Ça repose. C’est réglé. C’est chaotique… c’est au-delà de tout. La vie ici enfante les paradoxes.

Dix jours plus tard, ça va bien mieux dans la passoire de mon esprit. J’ai rencontré un peu de monde. J’apprivoise lentement la langue et les gens. Je décode la vie et les manières. Je tanne ma peau de petit blanc. J’ai rangé mes couteaux. Je bricole l’extraordinaire.

Ça a été dur. Je suis loin d’être le premier à débarquer ici. Sans but, sans pourquoi. À plier ma vie tranquille dans des cartons. À n’avoir dans mes bagages qu’un petit avion de papier. Un billet simple vers ailleurs. Seulement, je manque de courage. J’ai les bras courts. Je ne connais pas la langue de l’aventure. Le monde, dès qu’on pose un pied dedans, ça ne ressemble plus à un bouquin.<br /> 
J’en ai parlé ici. J’ai essayé de comprendre. De me comprendre. Ma réaction, mon délire. On se moque tendrement. On me rassure. J’ai eu un choc. Un simple choc. Banal.

Trop de zèle. Trop de bordel dans la bibliothèque du crâne. Trop gonflé. Trop orgueilleux. Et trop rapide. Et puis trop confiant. Trop de trop. Je me ramasse. Petite marionnette blanche qui croyait déjà tout savoir… un état de choc. Bête et méchant. Ça va mieux aujourd’hui. 

Première leçon de l’Afrique : l’humilité !!!

