+++
Author = "Lewo"
title = "Jeudi 13 octobre 2016 18h46"
date = 2016-10-13T18:46:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

C’est difficile de traverser cette vie-là sans crever à moitié.<br />
D’abord, c’est l’ennui. L’ennui qui, toujours, promène ses lames. T’as beau faire semblant d’être occupé, beaucoup, beaucoup, il est là. A perpétuité. Ce fiel ! Traître pute. Sanguinaire. Cannibale. C’est ton ombre. C’est le souffle froid dans ton dos. Terrible comme il en faut des imaginations pour qu’il t’ignore un peu. Mille et mille petits bricolages. De la besogne passionnante. Du remue-ménage. Avoir l’air, au moins, d’être toujours tout à ses affairements. Parce qu’il est là, lui, l’assassin. Prêt à bondir à la première fatigue. Au premier relâchement. Il est là, avide. Prêt à t’égorger l’âme !

Et puis, après, c’est l’égoïsme. L’égoïsme ! Putain, tu fais pas gaffe. Jamais eu la trouille. C’est pour les autres. Loin, loin de ta vie ces êtres-là... Et paf ! Canif ! Crocs ! Platane ! C’est pas lourd, ça vient tout doux. Deux, trois mots sordides, comme ça, gratuit. Deux, trois coups de mâchoire dans le coeur. Et voilà... réalité ! Boum ! fait la mine qui te pète au fin fond de l’esprit. Boum ! fait le masque qui tombe ! Dégoût...

