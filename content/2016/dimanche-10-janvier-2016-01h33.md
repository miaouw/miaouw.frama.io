+++
Author = "Lewo"
title = "Vendredi 25 Janvier 2019 18h11"
date = 2016-01-10T01:33:23+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

La semaine dernière a été violente. Le froid, la pluie, le ciel écrabouillé. Les mauvaises nouvelles qui tombent. Les téléphones qui n’en finissent pas de crier leur petite musique de vautour. Et la maladie par ici, et la mort par là… et la vie qui fait sa vie. Bref, semaine inutile, chaotique. Inintéressante. 
Et les jours passent, et puis bon…
Le soleil revient. Change de chemise. S’amuse. Fait la course avec les paysages. Les couleurs reprennent des couleurs. La ville se réveille. Et les sourires…


Week-end en famille. On a des choses à faire. Un peu de bricolage, un peu de cuisine, des trucs à fêter, un zoo à visiter et puis des soirées à remplir de palabres. On flâne. On se promène dans la douceur du moelleux retrouvé. Ouf ! 
C’est drôle l’Afrique. Ça te prend dans le fond de la viande et ça prévient pas. Ça fait mumuse avec ton âme. C’est tranchant une seconde, caressant juste après. C’est brut, fiévreux, sans transition. Si tu manques de souffle, tans-pis pour toi. Ce n’est pas une mauvaise mère, au contraire. Elle enseigne qu’avec un peu de temps, tout s’apprivoise. À commencer par soi-même. 

