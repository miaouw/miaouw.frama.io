+++
Author = "Lewo"
title = "Vendredi 12 fevrier 2016 12h07"
date = 2016-02-12T12:07:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

C’est un bar. Un petit bar. Ici, on appelle ça un maquis. C’est dans le noir. Dans l’écho du noir. Dans le profond. Tous pareils. Toujours. Pas d’enseignes. Pas de panneaux. Pas de pub. Il faut connaître le sombre pour trouver. Il faut être chat. Déchiffrer. Écarter la poussière. Deviner les maigres signaux lumineux appelant les naufragés du cafard. C’est un bar à l’ombre de l’ombre. Un bar plein de ceux qui, sur le radeau de leur vie, ont bouffé leurs mains pour être sûrs de ne plus jamais travailler.
C’est un bar de blues. Un bar d’épaves. C’est un bar où tu bouffes ta dernière cigarette. Tes derniers cent balles. Ton putain de sourire perdu. Et tes yeux dans le monde. Et tes yeux dans le vide. Et la bière qui passe...


...et la bière qui passe encore. Il y a de plus en plus de monde au comptoir. C’est de la musique qui se réveille. De la danse qui s’agite. Un tango qui s’allume. L’alcool qui commence à allumer ses feux de brousse. C’est le masque de ta sale gueule qui se casse la gueule. Un sourire qui pousse. Un rire qui va s’éteindre là pour déteindre ici. Une étincelle, et tout bouge. La vie en loque éclate. Se moque de tout. La nuit protège. La nuit enveloppe. La nuit qui sent la pisse et la sueur. La nuit baisée debout par des astéroïdes bourrés, rieurs, acides et dansants. Et la bière qui passe...


...et la bière qui passe encore. Chaude. Plate. Dégueulasse. A la table voisine, une carcasse d’humain. Il vide des bières comme on vide une poubelle. Se lève, va se vider dans une pute comme on bourre une poubelle. Revient satisfait, répugnant, commande des bières...<br />
C’est l’heure ou les portes des chambres de passe sont des ailes de colibris. Ça rentre, ça sort, ça titube, ça vomis du bruit. C’est l’heure crade. L’heure de la putain. L’heure ou les gamines ont toutes dix-huit ans et pas la maladie. L’heure ou le rimmel est la teinte même de la nuit. Les regards ont des crocs. Les sourires ont du vice. Les verres sont pleins de bave. Les cœurs sont en rut. Les billets se défroissent. Peu importe le prix. Cinq minute de baise artificielle. Cinq minute de cul frelaté. Même dans l’ordure. Même dans des draps déjà mille fois trempés. Même la bite enfoncée dans une guillotine. Peu importe le prix, l’illusion de l’amour n’en a pas... Et la bière qui passe...


...et la bière qui passe encore. La bière qui coule à flot dans le ventre de cette atmosphère animale. C’est un bar pour les paumés, pour les échoués de la vie. Un bar pour les corbeaux, les épouvantails gonflés à l’alcool. C’est comme un hôpital de musique, une morgue joyeuse ou un cimetière ambulant. On y vide sa solitude, on y vide des verres, on s’y vide les couilles, à l’envi. C’est selon... Le bonheur est à sa place : au sol, foulé du pied. C’est un petit bar sale. C’est un petit maquis dégueulasse. C’est un petit cercle immonde. Un petit cercle vicelard où la vie est synonyme de mirage et d’alcool et de danse et d’amour encore. Même faux, pourvu qu’il soit amour. Même déteint, même maquillé, même pourri. Ignoble mais doux. Doux, doux à crever.

