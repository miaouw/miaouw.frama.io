+++
Author = "Lewo"
title = "Samedi 15 octobre 2016 03h55"
date = 2016-10-15T03:55:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Le grand escalier. Tous les jours.... Clope au bec. Trois paliers.<br />
L’ascenseur en panne, pas de lumière.<br />
Soir comme un autre, les marches sont saoules. En bas, le gardien qui offre le thé. La nuit a du coeur. Bonhomme !
 
Demain, ils seront dix mille en plein jour. Dix mille à t’ignorer. Dix mille dans la machine… et le petit mec qui offre le thé… la fleur qui pousse au milieu du métal. J’suis tout blanc bec, mais ce soir, on chante ensemble. Dormir après... Loin du monde... Loin des croix... L’âme au balcon qui reprend la chanson... les rythmes dessus ! Les rythmes... les sucres du thé !

