+++
Author = "Lewo"
title = "Lundi 18 janvier 2016 11h31"
date = 2016-01-18T11:31:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<p>Petite pensée pour Mawine et son livre de dessin.<br />
<em><ins>Leçon numéro un</ins> : dessinez rapidement les petites choses quotidiennes qui vous entourent...</em><br /></p>

<div class="center">
<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/douche_gd.jpg" title="Douche chaude" class="zoombox zgallery002">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/douche_min.jpg" alt="Douche chaude" /></a>
</div>

<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/gaz_gd.jpg" title="Cuisinière multi-fonction" class="zoombox zgallery002">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/gaz_min.jpg" alt="Cuisinière multi-fonction" /></a>
</div>

<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/four_gd.jpg" title="Four à chaleur tournante" class="zoombox zgallery002">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/four_min.jpg" alt="Four à chaleur tournante" /></a>
</div>

<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/fauteuils_gd.jpg" title="Fauteuils bobo-cul-cul" class="zoombox zgallery002">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/fauteuils_min.jpg" alt="Fauteuils bobo-cul-cul" /></a>
</div>
</div>
<div style="clear:both;"></div>

	



















	

