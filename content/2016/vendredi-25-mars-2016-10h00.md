+++
Author = "Lewo"
title = "Vendredi 25 mars 2016 10h00"
date = 2016-03-25T10:00:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="left">
	<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/vieux_01_gd.jpg" title="Un vieil arbre pêcheur" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/vieux_01_min.jpg" alt="Un vieil arbre pêcheur" /></a>
        </div>
</div>
<p>
C’est un morceau de bois. Un arbre qui marche. Un vieux tronc fatigué. La vie et le travail lui ont bouffé les muscles. Il ne reste plus que les os et la peau. Une peau tannée, creusée, abîmée. Une peau de tambour. Avant, il était pêcheur. Pêcheur de poisson. Aujourd’hui, il est à l’ancre. Encalminé, toutes voiles repliées. Il se traîne sur une mer pour sardines à l’huile. Une mer de souvenirs. On ne parle pas la même langue, mais on discute quand-même. Des mômes s’improvisent interprètes. Ça n’arrange pas tout. Les sourires tracent les grandes lignes. Les mains dessinent le reste. On se devine. Photo ? Photo ? Non, pas photo...
<br /><br />
...trente minutes. Peut-être plus. Il ne bouge pas mon arbre. C’est le jeu. Je tremble sur la pointe de mon crayon. Maladroit. Tans-pis, trop tard, j’ai voulu jouer. Poker, au bluff. Ça fait marrer les gosses. Finalement, j’ai quelques bonnes cartes cachées dans la manche. Dessiner un arbre, c’est une chose. Dessiner un arbre qui te regarde bien en face, c’est troublant. Cette gueule profonde. Ces rides calligraphiques. Ces grands sillons d’énigmes, de sagesse et de malice. Ce visage de bibliothèque. Deux trous au milieu du cuir pour y planter les yeux. Des yeux qui fouillent quelque chose au fond de l’air. Des yeux qui guettent la mémoire. C’est un vieil homme planté tout droit. Un homme taillé dans le même bois dont on fait les pirogues. Un homme mystère. Un homme arbre. C’est un pêcheur de poisson et c’est la sève du fleuve qui lui coule dans l’écorce. Photo ? Photo ? Non, pas photo... j’ai rangé mes crayons. Il a hésité un peu. Il a fait passer de mains en mains. Puis il a refusé le cadeau en disant qu’il était trop vieux pour garder un cadeau. Enfin, je crois. Hochement de tête et sourire sans dents. Je m’en vais avec ça, vieille branche. Vieil arbre pêcheur. 
</p>
<div style="clear:both;"></div>


