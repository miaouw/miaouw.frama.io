+++
Author = "Lewo"
title = "Jeudi 07 janvier 2016 12h22"
date = 2016-01-07T12:22:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="center">
	<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/afterpluie_gd.jpg" title="Après la pluie - Esquisse" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/afterpluie_min.jpg" alt="Après la pluie - Esquisse" /></a>
	</div>
<p>Hier soir, après la pluie.<br />
En dix minutes à peine, avec ses petits pinceaux, le petit toubabou s'est fait des potes.</p>
</div>
<div style="clear:both;"></div>

