+++
Author = "Lewo"
title = "Mardi 05 janvier 2016 12h39"
date = 2016-01-05T12:39:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<p>Premier dessin, au petit déjeuner. Un jeune Margouillat.<br />
Mignon petit monstre. Véloce la nuit. Carrément amorphe le jour. Bouffeur de moustiques. Compagnon sympathique qui a une fâcheuse tendance à se laisser tomber des plafonds...</p>


<div class="center">

<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/margou_002.jpg" title="Margouillat 1" class="zoombox zgallery001">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/margou_002_min.jpg" alt="Margouillat #1" /></a>
</div>
<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/margou_001.jpg" title="Margouillat #2" class="zoombox zgallery001">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/margou_001_min.jpg" alt="Margouillat 2" /></a>
</div>
</div>
<div style="clear:both;"></div>














