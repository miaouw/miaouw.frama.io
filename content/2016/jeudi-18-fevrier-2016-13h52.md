+++
Author = "Lewo"
title = "Jeudi 18 fevrier 2016 13h52"
date = 2016-02-18T13:52:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="center">
<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/dead_zouki_gd.jpg" title="Il est mort la musique..." class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/dead_zouki_min.jpg" alt="Il est mort la musique..." /></a>
</div>
</div>
<p><br />
Caro, Marie, j'ai promis une mélodie. Un truc bien baisé, un truc d'ici... mais, non.<br /><br />
Un instrument de musique qui craque, ça fait exactement le même bruit qu’un cœur qui se déchire. <br />Personne pour le voir. Personne pour l’entendre...<br /><br />
Il est mort mon vieux bouzouq... Mon petit frère. Mon radeau. Ma mélodie intime. Ma déglingue folle. Mon pince temps. Il est mort la musique ! Tout seul et sans un cri, et sans une note. Il est mort de silence. Mort en faisant autant de bruit qu’une larme lancée contre un piano...</p>

