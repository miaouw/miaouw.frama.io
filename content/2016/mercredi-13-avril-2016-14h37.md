+++
Author = "Lewo"
title = "Mercredi 13 avril 2016 14h37"
date = 2016-04-13T14:37:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="right">
	<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/esquisse_loup_gd.jpg" title="Esquisse loup" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/esquisse_loup_min.jpg" alt="Esquisse loup" /></a>
        </div>
</div><p>
Pas beaucoup de nouvelles… manque de temps et surtout, manque d’électricité. On baigne dans la sueur de la saison chaude. La température explose. Le courant est plus que facultatif. L’eau fraîche vaut de l’or. La douche ne fait même pas de bien. Et toujours la soif, la soif, la soif... Que-ce-que tu veux foutre de la soif dans un pays sans eau ?!<br /><br />
En attendant les pluies, je commence un boulot… à l’école ! Y’a un spectacle de fin d’année à préparer. Une sombre histoire de loup, de cochons, de lapins, etc, etc. Me voilà directeur des créations artistiques et chef musicien. Le taulier des arts, tout blanc, tout dégoulinant et tout fragile au milieu d’une ronde d’enfants. Ils ont les dents pointues. Les griffes méchantes. Des cheveux sur la langue. La morve au nez. Me voilà donc dans la gueule du loup... </p>
<div style="clear:both;"></div>

