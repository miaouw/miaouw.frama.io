+++
Author = "Lewo"
title = "Mardi 19 janvier 2016 09h31"
date = 2016-01-19T09:31:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

La France est givrée. Blanc neige. Ouagadougou plombé. Rouge sang. Bamako dort encore. Noir ébouriffé.

Moi, je galope dans le matin. Arc-en-ciel. <br /> 
J’aime bien ces heures-là. Elles sont à moi. Pour moi. Belles. Étranges. Hésitantes. Brouillonnes. Rien n’est encore dessiné. Le monde s’étire. La ville baille. Le décor enfile doucement ses milliards de couleurs. Le bruit fait ses gammes. Le tumulte s’éclaircit la voix. Le rideau bouge. Un soleil s’allume. Trois petits coups...

C’est d’abord un murmure qui grimpe sur les planches. Un moteur qui tousse. Des géométries en pyjama qui craquent leur os. Puis un cheval rouillé qui traverse la scène à rebrousse-poil. Et puis une boutique qui s’ouvre. Et puis une autre. Ali-Baba se frotte les mains, en didascalie. Des oiseaux zébrés griffent le ciel, en aparté. Et puis les premiers figurants arrivent enfin. C’est des enfants !<br /> 
C'est des marmots, des gosses, des mômes aux pieds rouges. C’est des enfants qui vont réveiller le monde. Agiter le décor. Endormir le silence. Remuer la poussière. Secouer les indolents. Acheter le pain, les cigarettes, le kilo de farine, le galon d’essence, la tonne de riz... Ils n’ont pas le grand rôle. Ils ne sont pas sur l’affiche. Pourtant, ils seront là, du premier au dernier acte. C’est des petits gars, c’est des petites filles. Des arlequins sans âge, des gamins, des gamines. Des gamines surtout !

Des gamines partout. Des gamines qui découpent la buée matinale. Tout à l’heure, elles vont passer le balai, jouer du pilon, accommoder le poulet, chercher de l’eau, récurer les marmites, cirer les parquets en pierre, bercer les bébés, laver le sale, nettoyer le propre, recoudre le fil des heures. Enfin... courir partout, sans cesse, sans repos. Et jusqu’au soir, et pour trois sous à peine, elles seront là. Présentes. Agiles. Vibrantes. En coulisse. Hallucinantes petites étincelles qui mettent le feu aux poudres du matin. Fragiles petits pompiers qui iront éteindre les dernières cendres du soir, quand tout le monde dormira déjà.

C’est fou ce qu’elles ont de force. C’est dingue ce qu’elles portent. C'est délirant comme elles bougent, ces petites abeilles maigres. Ces petites choses invisibles qui font la respiration des heures. Qui battent le pouls des journées. Sans fin. D’un crépuscule à l’autre. Chaque jour et tous les jours encore. Jamais le droit à la fatigue. Jamais de pauses. Pour que dalle. Elles passent, elles virevoltent. Insaisissables et anonymes. Tellement là et pourtant tellement évanouies dans le paysage. Libellules filantes en boubous multicolores. Que si tu les regardes passer, elles te laissent dans les yeux des filaments de couleurs. Elles te laissent dans la peau des éclats de rire. Elles te laissent surtout comme un con, à ne plus savoir si tu dois pleurer ou te marrer...<br />
En tous cas, que si tu les regardes passer, que forcément, tu t’inclines !
		

