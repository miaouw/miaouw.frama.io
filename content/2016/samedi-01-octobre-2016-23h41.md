+++
Author = "Lewo"
title = "Samedi 01 octobre 2016 23h41"
date = 2016-10-01T23:41:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Page blanche. Feuille nue. Rien à raconter. Rien n’est racontable.<br />
Écrire. Gribouiller. N’importe quoi. Fouiller dans la ferraille. 
Sortir quelque chose. Les signes du vivant. Les signes du merveilleux. 
Allons... ce ne doit pas être si loin. Ça viendra...

