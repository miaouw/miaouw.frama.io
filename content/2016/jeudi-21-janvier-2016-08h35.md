+++
Author = "Lewo"
title = "Jeudi 21 janvier 2016 08h35"
date = 2016-01-21T08:35:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

20 janvier. Jour férié. Et pour cause : fête de l’armée.<br />
J’avale un café saupoudré par la douce mélodie d’un branle-bas matinal délicatement éructé par la virilité poilue d’un clairon plein d’ardeur. Un clairon droit dans ses notes. Un clairon en uniforme. Un mâle, un chouette, un brillant. Taillé dans le cuivre sanguinaire d’un obus de quarante. Le genre de clairon vigoureux, avec plein de muscles et des médailles autour. Un beau clairon claironnant comme ça mon colon, certain que ça te pénètre le troufion, ça te stimule la caserne, ça te met au garde-à-vous. Et sans broncher encore. C’est de l’arme lourde. De l’engin de guerre. C’est pas de la pédale. Même si parfois, bon, avec les copains du régiment, on est tout dentelle, on est pas contre un peu de tendresse, qu'on aime bien se tripoter les gammes, s’astiquer un peu le si-bémol... Mais question musique, c'est recta ! Ça ne fait pas un plis. Pas un postillon de travers. C'est de la partition écrite avec du jus de couille. C'est de la note carré, de la grenaille menée avec maestria. C'est de l'instrument de première classe. Une putain de sulfateuse à requiem. Un bazooka à Te Deum. Une grenade à fragmentation symphonique. Etc, etc... Bref... c'est du héro national ce clairon-là qui vient me cracher dans les oreilles.


Je vomis mon café. Cette petite musique de cons c’est l’histoire de ce pays, c’est l’histoire de l’Afrique entière. L’histoire avec un grand hache dans la gueule.


Ici, c’est en 1960 que le pays a eu droit à son indépendance. Li missié blanc il est gentil, il est bon avec Coco, ui, ui, ui ! A Bamako, il y a le boulevard de l’indépendance. Un long machin avec des voitures qui roulent dans tous les sens. Les champs Élyséens du pays. Une belle ligne droite qui commémore la fin du colonialisme. La liberté retrouvée d’un peuple, l’identité retrouvée d’un pays. C’est un symbole, un post-it pour la mémoire...
Et qu’est-ce qu’on a planté, bien gros, bien dodu, bien dégueulasse, bien visible, pile-poil au bout de ce symbole ? Le centre communautaire Français…


Je touille un autre café. Je chasse ce putain de clairon de ma tête. Dans quelques minutes, je décolle pour la campagne, la jungle, la brousse. Enfin loin de la ville et du bruit des bottes. Brassens m’accompagne…

