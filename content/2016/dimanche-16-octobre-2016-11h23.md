+++
Author = "Lewo"
title = "Dimanche 16 octobre 2016 11h23"
date = 2016-10-16T11:23:23+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

<p> Promenade océanique, au frais, dans le soleil.<br />
Le grand bleu qui 
remue sa marée, il est à moi. J’ai du varech plein les poumons. Les 
lèvres vaguement salées. Le soleil qui m’arrose la gueule. Le vent, et 
puis tout, et puis tout encore... c’est à moi !<br />
Rien, que j’en laisse.<br /> 
L’horizon, tout, de-là à de là-bas, c’est pour ma frime ! Même, voyez, 
le petit bateau blanc, là-bas ? Berceuse ! Loin, tout loin. C’est à moi 
itou. <br />
Je ronronne ! Enfin !<br />
Si seulement ça pouvait se mettre en flacon, 
des moments comme ça.</p>

<p>Coup de pied dans les coquillages. <br />
Coup de pied dans le souvenir de ce 
médecin qui est passé ce matin.<br />
Inconnu. Gros con. Gris comme sa blouse 
blanche. Il déboule. Fracas, à son aise. Médecin, tu parles ! 
Croque-mort ! Sinistre ! T’es juste enrhumé. Lui, il a la gueule du gars 
qui veut savoir de quoi t'es mort. Déjà il cherche l'étiquette, sur le 
gros orteil.<br />
- Comment il va ?<br />
Pas un oeil sur le lit... j’attendais les 
condoléances.<br />
- Il va... heu... Demandez-lui ?!<br />
- Bon, bonne journée.<br />
Et il se casse. Comme ça. Médecin... mes deux ouaip ! Ça me chauffe.<br /> Mon squelette 
fait dodo. Paisible, bercé par des tentacules de perfusions. J’en profite. Vite, 
vite, au frais. A l’embellie. Vite. L’océan est à deux kilomètre. Du 
souffle !</p>

