+++
Author = "Lewo"
title = "Dimanche 23 octobre 2016 16h54"
date = 2016-10-23T16:54:23+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Il y a des jours ou je ne suis qu’un gros morceau d’enthousiasme. Ça 
fait tâche sous les plafonds de l’hôpital. Z’aiment pas la couleur. 
Dites... je vais me gêner ! Et je saute si je veux dans les flaques de 
sang. Pieds joints encore. Tralala ! J’ai le rire... le sourire... c’est pas 
tout le monde... Même les projets, j’ose ! Vous verrez... j’hésiterai à 
rien.

Un jour va venir où je pourrai gueuler : mes frères, mes sangs, je suis 
là ! Les bras grands ouverts qu’on va s’accueillir. S’embrasser. 
S’accolader. Se perdre les mots. Pleurer... non, tu parles ?! Se retrouver après 
de tels infinis... ? Pleurer, ouaip ! Et boire, après, pour bien goûter 
cette émotion de nous. Les projets ? Simples : vous, moi, nos 
capharnaüms, tout le reste de la vie qu’on va se baiser... et puis c’est 
marre !

Bon... après... voilà ! Un médecin, deux infirmières, trois nuages. Le vent 
qui se lève. Crac ! Plomb ! L’enthousiasme, c’est comme l’amour, ça fait 
du bruit, mais ça dure pas.

