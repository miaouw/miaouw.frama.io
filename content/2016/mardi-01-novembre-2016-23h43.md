+++
Author = "Lewo"
title = "Mardi 01 novembre 2016 23h43"
date = 2016-11-01T23:43:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Le marrant sort où je suis chu, s’il ne me fait pas barrer 
définitivement dingue, certain qu’il me fait saigner l’âme. La viande 
que j’y laisse... faut voir l’hémorragie ! Tout se casse la gueule. Tout 
est friable. Que même les mots, j’ose plus y toucher. Ne sont plus que 
plaintes. Bêlements. Cafards. Le verbe drogué à l’élégie... qu’est-ce que 
tu veux foutre avec ça ? Y’a rien à lire ! 

La violence, le 
sang, les viscères, les hurlements, les impatiences, les haines, les 
rages, les aiguilles, la pisse, la merde, les crachats, le temps, les 
solitudes, le désespoir, la mort, les suintements, les odeurs, le 
mensonge, le mépris, les horreurs, la peur, le sang encore... et quoi ? 
Des pages comme ça si vous voulez ! Là ! Dedans ! Je traverse. Debout. 
Chancelant parfois, mais debout. C’est toute ma vie que j’ai donnée. Pas 
un peu, pas à moitié, non, tout ! En plein dedans. Tout le tunnel, sans 
voir de lumière. 

J’en connais des tarés qu’on aurait retrouvés 
suspendus pendus pour moins que ça. Juste à l’idée de donner un peu, 
déjà, ils auraient crevé ! D’accord, je baisse mon froc. Je me la 
compare. D’accord, je suis là, tout con... tout blet... anéantit... 
ressasseur... Regarde cette page ! D’accord ! Mais je suis toujours là ! 
Toujours ! Pour lui... 

Ça doit pas suffire que de ne plus vivre 
pour soi. Que de traverser ce gouffre. Non, maintenant, mieux qu’une 
balle, c’est un mot qui vient essayer d’achever le travail... un mot 
froid, dur, impénétrable et qui sonne comme des barreaux de prison : 
clandestin.

Comment dire ? L’usure ! 

