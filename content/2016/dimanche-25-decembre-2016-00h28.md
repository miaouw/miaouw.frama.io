+++
Author = "Lewo"
title = "Dimanche 25 décembre 2016 00h28"
date = 2016-12-25T00:28:23
Categories = ["2016"]
menu = "main"
draft = false
+++

<p>Au soir...<br />
<br />
La vie...<br />
Un Picasso en carton.<br />
L’amour<br />
Dans des verres à grenade.<br />
La vie...<br />
Comme un vieux clope<br />
Au fond d’un cendrier.<br />
<br />
Télégramme :<br />
<br />
Pense à toi <br />
- Stop -<br />
Impossible crier<br />
- Stop -<br />
On a des frères partout,<br />
Quand on se mouille...<br />
- Stop -<br />
L’avenir,<br />
Ses charades...<br />
- Stop -<br />
<br />
La vie… un télégramme…<br />
- Stop - </p>
