+++
Author = "Lewo"
title = "Mardi 25 octobre 2016 02h22"
date = 2016-10-25T02:22:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Et puis quoi ? On leur dira… qu’on s’est jamais quittés ?!<br />
C’est juste que... voilà...<br />
Tes perfusions, <br />
Les menottes à mes poignets,<br />
C’est rien ! Sinon mes promesses, sinon nos doutes... <br />
Demain, tout est sur un nouveau chantier.<br />
Et pour le facteur ? <br />
On dira rien...<br />
On a changé d’adresse. Pas de boîte à coeur !<br />
Cher facteur, il faut couper nos lettres en deux...<br />
Et puis qu’ils aillent...

