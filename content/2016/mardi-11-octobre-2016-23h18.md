+++
Author = "Lewo"
title = "Mardi 11 octobre 2016 23h18"
date = 2016-10-11T23:18:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Une bière. Une pinte de bière. Fraîche. En pression. Blonde, brune, rousse...<br />
Ça pèse pas lourd, un gramme de rêve sur du papier.<br />
Ajoutez un clope. Roulé petit, peu tassé.<br />
Deux, trois frangins à la table. <br />
Puis la nuit qui vient, pour réchauffer.<br />
Et puis des souris qui traversent ce paysage.<br />
Feux d’artifices. Mutines petites comètes. Blondes, brunes, rousses...<br />


