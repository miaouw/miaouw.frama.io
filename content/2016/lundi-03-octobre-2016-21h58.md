+++
Author = "Lewo"
title = "Lundi 03 octobre 2016 21h58"
date = 2016-10-03T21:58:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Un chien. C’est ça ! je suis un chien ! un chien arabe ! j’aboie dans toutes les langues.<br />
Vingt fois par jour, c’est l’heure de la promenade. Trottoir. Petits pas. Tour en rond. Mégot dans les babines. Toujours. Deux dirhams le coup de goudron. À ce prix-là... fume toutou !  

Poil humide. Queue basse. Je me traîne dans les couloirs de la ville. Jamais loin des caniveaux. C’est la grisaille qui tient la laisse. L’ennui qui pousse au trouf’.<br /> 
Errances poisseuses.<br />
Mornes.<br />
Régulières.<br />
Avec les beaux jours quand même, truffe plantée dans le cul des femmes. 


Ce cul qui passe là, c’est ma girouette. Mon cou-couche-panier.<br />
Je donne la patte. Je fais le beau.<br />
Maîtresse, appelle-moi caniche.<br />
Je t’appellerai caresse, canine, collier, gamelle, joujou, croquette, exquise, minou, fourrière... Prends ma laisse, capture-moi, siffle-moi. Au pied !<br /> 
Sinon je pleure, sinon je hurle. Sinon je mords. 

Deux, trois lampadaires plus tard, l’imagination a fini son os... retour à la niche.<br />
Le chagrin, faut le rentrer avant qu’il morde.

La niche... néons, peinture blanche, perfusions, sirènes, éther, etc, etc...


Un chien ! Non ! c’est plus humain que ça, un dog.<br /> 
Hors du vivant en tous cas, hors du règne ! J’ai quitté l’enveloppe !<br />
Je ne dors plus, ne mange plus, habite dans une brume, vie dans une vapeur, agite des pensées de fantôme. La vie à reculons. Je me ratatine comme la vieille dame d’à côté.

La dame ? Chose ! Elle est vieille. Elle est moche. Elle suinte. Elle a de la mouche dans la viande. Elle est morte presque déjà d’elle-même. Ça geint, grince, coule, n’en finit pas de se plaindre. 
Ça fou le camp, dégueulasse.<br /> 
Ma gueule tout craché quoi ! Ad nauseam...            

