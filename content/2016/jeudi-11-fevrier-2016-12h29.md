+++
Author = "Lewo"
title = "Jeudi 11 fevrier 2016 12h29"
date = 2016-02-11T12:29:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Deux mois de broussaille sur le visage. La gueule en savane. Je me cherche dans les miroirs. Je rêve rasoir. Fine lame. Acier bleu. Je rêve d’un figaro. D’un vrai. Comme dans les films de Sergio Leone. Fauteuil renversé, en cuir. Mousse épaisse et serviettes chaudes. Un coupe-choux mexicain sous la gorge... 


Soleil dégoulinant, pagaille de mouches, poussières lourdes. Le temps est idéal. Sur le bord d’un goudron, une cahute : Elvis Coiffure-Rasage. Parfait ! Mon film se met en place. J’y vais désarmer, un harmonica dans la tête. 


Je pousse des deux mains l’inexistence d’une porte qui ne grince pas. Travelling avant. Au sol, des cadavres de cheveux. Aux murs, des posters de Rocky Balboa. Dans un coin, une télé. Mon scénario s’effrite. Dans un autre coin, le fameux Elvis, clope au bec. Taillé comme un boucher. Habillé comme un boucher. Pas mexicain du tout. Gros plan sur ses yeux. Gros plan sur mes lunettes. Il va y avoir bagarre. Plan large. Longueurs moites. Suspens terrifiant. D’un coup, il dégaine une tondeuse électrique... 


Plan séquence avant le générique de fin. Je m’en vais dans le soleil pas couchant. J’ai retrouvé mon visage.<br />
Mon petit rêve de western lui, il s’est fait trouer la peau par une tondeuse électrique encore fumante. Trois cent balle dans le bide, ça pardonne pas. Il est resté au sol, agonisant dans une marre de poil... drôle de film !
J’ai du me tromper de continent. Fin.

