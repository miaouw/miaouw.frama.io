+++
Author = "Lewo"
title = "Lundi 12 décembre 2016 01h38"
date = 2016-12-12T01:38:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

<p>
<img src="https://download.tuxfamily.org/miaouw/statique/img/fusain_01.jpg" alt="Une étoile pose à poil" class="left">
Doucement<br />
Les bouteilles se couchent<br />
Dans la fosse commune.<br />
Doucement<br />
La nuit revient,<br />
Avec son couteau de lune<br />
Entre ses dents de chien.<br />
Elle va me le planter dans le dos,<br />
L’ennui.<br />
Le rien.<br />
Le vide.<br />
Alors j’imagine...<br />
Et, en quatre coups de fusain,<br />
J’assassine<br />
Une étoile<br />
Qui pose à poil,<br />
Le cafard qui traîne,<br />
L’habitude du peu<br />
Et sa gangrène. 
</p>
<div style="clear:both;"></div>


