+++
Author = "Lewo"
title = "Mercredi 06 janvier 2016 12h14"
date = 2016-01-06T12:14:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
+++

<em>Température : 15°C<br />
Humidité : 95 %<br />
Vent : 20km/h<br />
Habits : chaussettes et gros gilet</em>

Ça frisonne drôlement. Ça tombe des cordes. L’eau est rouge. La rue déborde. S’abrite. S’inquiète. Lève les yeux. Un homme la traverse, pieds-nus, un coq sous le bras. On hallucine. On se gratte les paupières. On murmure. C’est pas normal ça ! Y’a quoi ?!

Deux jours que la ville est un aquarium. Un glaçon posé sur un volcan. Janvier, c’est pourtant loin de la saison des pluies. Je n’ai pas vu beaucoup d’hectares de ciel bleu depuis mon arrivée. Mais alors là... et la pluie, et le froid, et le vent... dingue !

