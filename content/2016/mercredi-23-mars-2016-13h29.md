+++
Author = "Lewo"
title = "Mercredi 23 mars 2016 13h29"
date = 2016-03-23T13:29:00+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++

<div class="left">
	<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/taxi_girl_gd.jpg" title="Taxi girl" class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/taxi_girl_min.jpg" alt="Taxi girl" /></a>
        </div>
</div>
<p>
Crade, morveuse et pieds nus. Avec des yeux immenses. De quoi bouffer le monde. Avec un sourire immense. De quoi faire rougir le monde. Elle a dix ans la môme. Même pas.</p>

<p>Elle a l’age des gosses qui ont des voiliers dans les pupilles. L’age où si tu veux manger un fruit, tu l’imagines, tu le dessines, tu le pèles et tu le manges. L’age où dans les caniveaux coule de la grenadine et où les nuages sont des dessins d’animaux animés. L’age où tout est possible, même voyager à bord d’un cerf-volant, même grimper dans ce putain de taxi et filer très loin. Et pourtant...</p>

<p>J’avais envie de lui inventer une histoire à cette poupée. Malheureusement, je sais déjà une chose : elle n’y montera jamais dans ce taxi. Jamais. Mauvaise étoile...
</p>

<div style="clear:both;"></div>




