+++
Author = "Lewo"
title = "Lundi 11 janvier 2016 13h19"
date = 2016-01-11T13:19:20+01:00
Categories = ["2016"]
menu = "main"
draft = false
art = true
+++
        
<div class="right">
	<div class="zoom">
<a href="https://download.tuxfamily.org/miaouw/statique/images/miw3/muso_01_gd.jpg" title="Belles, toutes, trop..." class="zoombox">
<img src="https://download.tuxfamily.org/miaouw/statique/images/miw3/muso_01_min.jpg" alt="Belles, toutes, trop..." /></a>
        </div>
<p style="text-align:center;"><span class="small"><em>PS : La musique de ce dessin est signée David Bowie...</em></span></p>	
</div>
<p>
Je crois, mon cher vieux capitaine, qu’on va avoir des choses à se dire, un jour... <br /><br />

Les soirées vont êtres longues, les verres trop petits. <br />
C’est qu’il est turbulent, ce petit poisson rouge qui tourne sans fin dans l’aquarium de nos émotions. Elles sont là, elles se promènent, en vrai, en songe. Nos fantômes ondoyants. Nos folies folles. Nos insaisissables petites berceuses. <br />
Et les étoffes, et les odeurs. Et les fragiles, et les lueurs. Et les mystères de leurs abîmes. Et mes cons de mots qui rimes. <br /><br />
Ah ! Belles, toutes, trop...<br />
</p>
<div style="clear:both;"></div>





