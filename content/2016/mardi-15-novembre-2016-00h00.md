+++
Author = "Lewo"
title = "Mardi 15 novembre 2016 00h00"
date = 2016-11-15T00:00:00
Categories = ["2016"]
menu = "main"
draft = false
+++

Bon... de retour alors ?! Et libre ! C’est pas rien ça ! Quoi... si on veut.
Seulement, d’un coup, comme ça, moi je ne sais 
pas trop quoi en foutre de ma liberté. Finalement, l’hésitation, ça 
revient vite. Vilaine plaie.

Ne rien écrire de plus. Laisser mijoter encore un peu... se cuire. 

En attendant ? La rue ! Le rire inépuisable des troupeaux de poivrots 
allant à l’abreuvoir. C’est toute la musique qui me reste. Et ma soif de 
vieux loup, au diapason.

