+++
Author = "Lewo"
title = "Jeudi 08 Avril 2021 18h18"
date = 2021-04-08T18:18:40+02:00
Categories = ["2021"]
menu = "main"
draft = false
art = false
+++
S’endormir, penser à demain et toujours se réveiller dans ce cauchemar qu’est aujourd’hui. Putain ! Ça manque d’originalité !!!  

Et ce grand creux, là, dans la caboche ! Dam’ !  
L’imaginaire... Les nuances et les couleurs. Les oscillations. L’insolite, le capricieux, le déraisonnable, le beau et tant, et puis tant encore... Tout est en tout petits morceaux. En tout petits fagots. La belle brassée ! C'est le temps qui les enduits de poix ! La raison, elle, est déjà au poteau. Liée...  
Et dans la nuit s’avance une torche...
