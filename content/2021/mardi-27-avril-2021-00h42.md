+++
Author = "Lewo"
title = "Mardi 27 Avril 2021 00h42"
date = 2021-04-27T00:42:57+02:00
Categories = ["2021"]
menu = "main"
draft = false
art = false
+++
On discute. Enfin... elle parle !  
Moi, je barbouille. Je grince et vire au carmin. Tout flottant et mal enchevêtré, comme d’habitude. Et la mouillure qui s’en mêle et puis l’envie de caleter sec ! Figé pourtant, avec le bourrichon qui tricote dans son coin à s’en péter une artère !!! Ça galope ferme, à la limite de la rupture pour trouver un truc à dire. Un truc à dire...  
Elle me donne un bidule qui doit-être un numéro.  
Puis un machin qui doit-être une adresse mail.  
J’sais pas trop...  
J’ai juste entendu : "<em>toute attachée</em>"...
