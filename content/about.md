+++
title = "A propos..."
Author = "Lewo"
date = 2014-01-01T00:02:00
draft = false
notoc = "true"
type = "wtf"
layout = "wtf"
hidden = "true"
+++

Le petit chat est mort... miAouw est mort !  
Ne reste plus que ce petit blog personnel coincé dans les sous-terrains de la cinquième dimension... 

<h3>Mentions légales :</h3>

Vous êtes parfaitement libre de jouer avec le contenu de ce blog selon les conditions suivantes (*sauf mention contraire*) :

-- Les *textes* publiés sur ce blog sont sous licence **[CC4-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr "CC4-BY-NC-SA")**.  
-- Les *photos* et les *dessins* publiés sur ce blog sont sous licence **[Art-Libre](https://artlibre.org/ "Art Libre")**.


### Contact :

<p>Pour entrer en contact avec la cinquième dimension, envoyez un mail à l'adresse suivant : <span class="inv_mail"><span id="maile" class="arobaze">PLEASE-ACTIVATE-JAVASCRIPT--TPIRCSAVAJ-ETAVITCA-ESAELP</span></span><script type="text/javascript">window.document.getElementById("maile").innerHTML = '[A]';</script></p>


### Un peu plus...

Miaouw carbure avec une bonne dose de [Hugo](https://gohugo.io/ "Hugo") concentré à ~0,51%.  
Le bras mécanique qui touille le chaudron magique ainsi que l'ensemble des tapisseries ont été bidouillés par [Lewo](https://miaouw.info "miAouw").  
Le chat traverse les dorsales de l’océan numérique grâce à [TuxFamily](https://www.tuxfamily.org/ "Tuxfamily").  
Flux RSS : [https://miaouw.info/index.xml](https://miaouw.info/index.xml "flux rss").


