+++
Author = "Lewo"
title = "Mardi 31 juillet 2018 23h23"
date = 2018-07-31T23:23:00
Categories = ["2018"]
menu = "main"
draft = false
+++

J’hypothèque demain.<br />
Ça vaut combien mes jours ?<br />
Je pos’ tout, je r’tiens rien,<br />
Je r’fais le papier peint<br />
De notre vieil amour.<br />
C’est demain contre hier...<br />
J’ vais m’ refaire !


