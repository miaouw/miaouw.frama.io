+++
Author = "Lewo"
title = "Lundi 02 avril 2018 23h11"
date = 2018-04-02T23:11:00
Categories = ["2018"]
menu = "main"
draft = false
+++

A quoi ça mène de ne pas écouter l'enfance ? A quoi ça nous avance ?<br />
T'as vu... un jour, comme ça...<br />
C'est pas solide, c'est pas grand chose. Y'a rien ! Et pourtant, parfois, dans les ténèbres, y'a une lumière qui commence à gagner...<br />


