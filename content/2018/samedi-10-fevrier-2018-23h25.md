+++
Author = "Lewo"
title = "Samedi 10 février 2018 23h25"
date = 2018-02-10T23:25:00
Categories = ["2018"]
menu = "main"
draft = false
+++

Elle, elle navigue dans la pudeur. C’est mignon, peut-être très 
joli, vraiment ! Même si ça manque un peu de lumière... Coiffée vaguement, 
légèrement maquillée, en habits nuageux, avec ce truc qui traîne au fond 
des yeux... une déchirure adorable !

Moi, je t’illuminerai toute. J’ai des soleils à donner tu sais ! Des 
fontaines flamboyantes, des éclats, des parfums... milles petites 
vengeances aussi.<br />
C’est méchant putain, mais c’est si bon !


