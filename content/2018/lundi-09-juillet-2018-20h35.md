+++
Author = "Lewo"
title = "Lundi 09 juillet 2018 20h35"
date = 2018-07-09T20:35:00+01:00
Categories = ["2018"]
menu = "main"
draft = false
+++

Bha mon vieux... ! T'es déjà presque en route, ton petit rêve bercé entre
le ciel et la mer. Ton petit rêve... enfin !!! Preuve que les naufrages, 
c'est plus souvent au fond de l'âme que sur le dos des océans.

