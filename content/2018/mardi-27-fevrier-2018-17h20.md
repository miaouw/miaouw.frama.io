+++
Author = "Lewo"
title = "Mardi 27 février 2018 17h20"
date = 2018-02-27T17:20:00+01:00
Categories = ["2018"]
menu = "main"
draft = false
+++

<img src="https://download.tuxfamily.org/miaouw/statique/img/souris_001.png" alt="souris" class="right"><br />
Le soleil de février 
semait des fleurs blanches dans les gouttières du printemps. J'avais au 
moins mille ans...
Tes lèvres ! Ton corps ! Nos murmures... cette nuit !<br /> 
La plus belle des saison ! Les aveux câlins, les fleurs rêveuses, les 
étoiles mutines, nos mains... quoi encore ?! Mignone petite chose, étrange petite souris... quoi, encore ?!<div style="clear:both;"></div>


