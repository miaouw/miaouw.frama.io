+++
Author = "Lewo"
title = "Lundi 08 janvier 2018 22h23"
date = 2018-01-08T22:23:00+01:00
Categories = ["2018"]
menu = "main"
draft = false
+++

<div style="-moz-transform: skew(-1deg, -1deg); -o-transform: skew(-1deg, -1deg); -webkit-transform: skew(-1deg, -1deg); transform: skew(-1deg, -1deg);">
<p>"<em>Ce n’est rien, Seigneur.  C’est une femme, sur un trottoir, qui passe et qui gagne sa vie parce qu’il est bien difficile de faire autrement.<br />
Un homme s’arrête et lui parle parce que vous nous avez donné la femme comme un plaisir.<br />
Et puis cette femme est Berthe, et puis vous savez le reste.<br />
Ce n’est rien.  C’est un tigre qui a faim...</em>"</p>
</div>
<div style="-moz-transform: skew(+1deg, +1deg); -o-transform: skew(+deg, +1deg); -webkit-transform: skew(+1deg, +1deg); transform: skew(+1deg, +1deg);">
<p class="right">"Bubu de Montparnasse" – Charles-Louis Philippe</p>
</div>
<div style="clear:both;"></div>
