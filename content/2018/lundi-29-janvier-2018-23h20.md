+++
Author = "Lewo"
title = "Lundi 29 janvier 2018 23h20"
date = 2018-01-29T23:20:00+01:00
Categories = ["2018"]
menu = "main"
draft = false
+++

J’ai cru en toi. Tu sais ?! En nos poèmes. En tes adolescences. Et 
puis aussi à l’encens... aux métaphores. A nos rêves sur ton ventre. A 
cette putain de sève grimpante...

J’ai cru en toi... en nos paresses. Nos bateaux... Et l’vent du 
nord... tu sais... l’opéra de nos yeux ! Tu t’souviens ? Le vert, le 
bleu, l’animal ?! Les oiseaux fous. Les hautes tours. Les barricades. 
Les désirs mille fois rapiécés... et puis nos brumes lointaines... ! et 
puis la mort de nous, enfin !

Ici, t’es rien qu’une pauvre frimousse. Une photo dégueulasse qui
traîne dans ma mémoire. Un vieux machin magnifique avec un prénom de
légende... ma p'tite girl ! Ma souris...<br />
Tes émancipations, mes vagabondages, nos violons... nos fureurs océanes... au feu !<br />
C'est l'temps qui mesure la tendresse. C'est l'temps qui fracasse tout !

Tout a changé ici, dedans-moi, tu sais, les incendies, les cascades, etc, etc... j'suis
maintenant loin d'être nous. Putain, l'bordel !



