+++
Author = "Lewo"
title = "Mercredi 31 janvier 2018 18h32"
date = 2018-01-31T18:32:00+01:00
Categories = ["2018"]
menu = "main"
draft = false
+++

Elle était comme une fleur dans une cage. Oh la jolie rousse ! 

Cent-millième de seconde. Madame qui passez... cent-millième de seconde. A 
peine... ! c’est déjà vachement l’éternité ! Mes yeux, mes mirages. 
L’orage qui roule, les feux d’alarme, le vent debout, la pensée qui te 
devance, et puis tout encore... Madame cent-millième de seconde. C’est 
ma peau contre tes sortilèges. D’avance, la fracture.

Madame ma rouquine. T’es qu’un ventre de fille. Tu t’appelles la nuit, déjà. T’es 
l’océan. J’ai les émotions qui roulent bord sur bord et la mort lente. 
Mes doigts pris dans tes cheveux d’Irlande.<br />
Rouquine comme une ombre...


