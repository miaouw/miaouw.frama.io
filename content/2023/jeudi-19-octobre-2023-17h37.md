+++
Author = "Mathul"
title = "Jeudi 19 Octobre 2023 17h37"
date = 2023-10-19T17:37:33+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Somme toute, je suis gentil. Bon garçon, un brin chic, drôle, calme et croquignolet...  
Fichtre !!! Si vous saviez ce que j’en pense !  

Tout enfoncé dans une sale guerre contre une salope de croque-mitaine. J’imagine que ce sera mieux un jour, dans la paix... forcément ! Mais il est des instants ou je bouffe cet espoir-là comme un bonbon et puis que c’est rien quand même que de la merde. Que la guerre elle va pas finir. J’ose pas dire, pour dégoutter personne... mais j’y songe ! Un jour ça va gueuler. Je vais casser le morceau devant tout le monde, à commencer par la sainte famille.  
Un jour on en a marre de tourner en toupie dans la merde. La mouscaille ça va cinq minutes !  
Après... après le monde de me trouver bien mal élevé d'un coup.  
Et puis c’est tout.

