+++
Author = "Mathul"
title = "Lundi 23 Octobre 2023 20h58"
date = 2023-10-23T20:58:09+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Elle est déjà vieille à quarante berges. Très con, aimable comme un bouton de fièvre et préposée au service du petit déjeuner. Moche et laide, faut voir comme... L’irréparable outrage n’y est pas allé de main morte... Toute osseuse et ridée, imbécile à vu d’œil et même d’aspect franchement bigote.

Ce matin elle a éclaté tout un bol de café juste en le regardant. Pile devant ma poire... ! Merci ma rombière ! Tu sais, moi j’ai le rire facile, naturel... l’embellie dans le fiel. Et heureusement : c’est pas tout le monde ! Les moments de joies sont, à notre image, mortels. Feux d’artifice d’artifices... mais ça me fait encore sourire ce soir !
