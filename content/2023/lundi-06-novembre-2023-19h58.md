+++
Author = "mAdours"
title = "Lundi 06 Novembre 2023 19h58"
date = 2023-11-06T19:58:36+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Bien sûr Paulo que les choses sont pénibles, mordantes, fracassantes même encore. Bien sûr que tu chiales la nuit et que tu voudrais bien hurler le jour. Bien sûr que tu connais ce chemin sans étincelle. Bien sûr que la douleur des autres c’est la tienne. Bien sûr les errements de l’âme... bien sûr que je ne sais pas quoi te dire, ni quoi faire. Etre, c’est déjà pas mal. C’est un petit peu. C’est peu mais c’est plus vivant que le vide. Nos couleurs, nos bords intimes... j’y songe souvent tu sais... Si tu lis ces mots, sache une chose : je t’aime mon grand Paulo !!!
