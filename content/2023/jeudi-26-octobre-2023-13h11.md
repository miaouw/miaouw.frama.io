+++
Author = "Mathul"
title = "Jeudi 26 Octobre 2023 13h11"
date = 2023-10-26T13:11:03+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
C’est une fatigue qui n’a pas de nom, de celle qui vient avec l’angoisse toujours recommencée. Ça réclame des acrobaties, des pirouettes et des tours de tartempion pour pas se laisser avaler. Le temps passe sans jamais vieillir lui. Il s'en moque. Mais ma binette à moi, elle prend les rides et les cernes et puis les coups. Et sans pouvoir les rendre encore. Cette fatigue, cet accablement c’est à se demander si ça va finir un jour. Ça fait flétrir grand train ! J’ai juste envie de dormir quelques éternités et m’éveiller un jour, pimpant ! En attendant : mettre les gants, aller boxer...
