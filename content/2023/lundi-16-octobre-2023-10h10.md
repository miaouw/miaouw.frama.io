+++
Author = "Mathul"
title = "Lundi 16 Octobre 2023 10h10"
date = 2023-10-16T10:10:00+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = true
+++
<a href="/img/ratgd.png" title=" " class="zoombox" loading="lazy">
<span class="zoomomie"><img src="/img/ratmin.png" alt=" " width="300" height="205" class="leftmo" loading="lazy" /></span></a><br />

L’angoisse. La dépression. Tu parles d’un état d’âme ! C’est une violence, une salope de maladie qui ronge le bide, qui rayonne dans l’âme. Une pute qui dure, qui dure encore et s’installe. Jusqu’a devenir une partie de soi. Jamais tu t’habitueras. C’est toujours aussi neuf, aussi sauvage, violent. C’est comme ça, c’est pour toujours. Tout a un goût de mort, tout !  

Le pire c’est que c’est l’histoire d’un premier regard. Comme un amour dingue ! On s’imagine seul au monde et puis... non. Un jour on s’aperçoit qu’on est vraiment seul... sans l’autre. C’est ça le vice, la tâche sur le scanner. Tu penses devenir, réaliser... mais elle est là toujours ! Cachée, têtue, ténue. Tu le sais pas encore... Même les projets jamais réalisés tu oses y croire. Et les jours de fêtes... même les promesses jamais tenues, finalement c’est ça qui te fabrique peut-être, qui fonde en profondeur. Que tu sais que la vie galope. Il n y a qu’une seule partie à jouer, qu’il faut profiter. Que le mieux qui reste à faire c’est de rendre vrais les rêves d’enfance... Mais elle est là...  
Nous sommes tous des putains de mutilés de nos rêves, et on ne trimballe pas impunément tout un cimetière avec soi...
