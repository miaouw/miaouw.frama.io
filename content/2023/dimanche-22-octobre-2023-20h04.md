+++
Author = "Mathul"
title = "Dimanche 22 Octobre 2023 20h04"
date = 2023-10-22T20:04:13+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
L’esprit. La mélasse que c’est ce machin ! Et les questions qu’on me pose. L’esprit...  
Pourquoi partout ça se déchire, se déglingue, se délite, s’en va pourrissant... ? Qu’est-ce que j’en sais moi ?! Je suis marqué peut-être ?! Que j’ai beau me débattre, ruser, baratiner avec le destin... de toutes les façons possible je me retrouve fleur ! Hors la vie, hors la beauté, hors le monde, hors la santé même. Du côté des ahuris. Du côté des lamentables et des funestes de quelques façons dont je me retourne. Avec le sourire encore, pour pas déranger. Et qu’on me pose des questions sans cesse et sans réponses...  

Ca me débecte les questions ! On m’en a trop posé je crois. Et sur tous les tons encore... comme ça, brusquement, à brûle-pourpoint, bêtement, benoîtement, en chafouin, durement. Je voudrais une bonne fois pour toute réponse dire : merde !!!
