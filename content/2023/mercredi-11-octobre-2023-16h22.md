+++
Author = "Mathul"
title = "Mercredi 11 Octobre 2023 16h22"
date = 2023-10-11T16:22:44+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
J’étais l’enfant du jamais.  
L’enfant du jamais fait, du jamais dit, du jamais là. Le gosse effacé, déjà fantôme. Si maigre. A peine enfant et déjà presque au bout de l’habitude, là où meurt l’imaginaire. Et maintenant ? L’ombre d’un sac d’os... sans avis, sans envies, sans caprices, sans desseins.  

Pourtant j’aime le brouillard : on y voit rien, alors on invente...  
Où sont les songes et les couleurs... ?! 
