+++
Author = "Mathul"
title = "Dimanche 15 Octobre 2023 15h13"
date = 2023-10-15T15:13:01+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++

Merci grand Paulo pour ce retour en arrière de presque... dix ans (~2014) !  

<em>"Il faudrait que quelque chose arrivât...  
Dans ma vie, dans ma propre vie, je me sens toujours comme le gars pas du coin tombé dans un bistrot d’habitués. J’ai souvent l’impression que le moment que je suis en train de vivre, le présent, ne compte pas. Que c’est juste une simple préparation à un lendemain qui, lui, sera le vrai moment. Le seul. Le pleinement vécu. Aujourd’hui c’est du vent. Ce n’est qu’une répétition, un brouillon, une grisaille… un truc flou et sans importance. En attendant je fais mes gammes, tranquillement. Je m’échauffe. Je trie les pièces du puzzle. Je fais le tour du bocal. Je boucle ma valise sans me poser de question, puisque c’est demain et bien demain le grand commencement. Aujourd’hui je m’en moque, c’est demain qui sera tout. La belle promesse. Le grand jour de fête. Le lumineux vertige. Le départ du bateau. Le premier jour de la vraie vie. Le réel, c’est demain.</em>  

<em>Evidement, je me casse la gueule. Chaque demain n’est qu’un aujourd’hui qui prépare un autre demain et qui se mord la queue. Jamais je ne rattrape la carotte. Je manque de souffle. Je vis en décalage. Je ne sais même plus si j’existe et, quand j’y pense, ça me fout la panique dans le ventre et le mal au crâne dans le crâne. J’entends alors cette lancinante petite musique atroce qu’est la survivance d’un passé qui ne fut pas vécu. Ça fait bobo en dedans. Ça donne une vie dans les tons gris sale, mais une vie. C’est alors le moment de rebondir. De se réveiller. De se donner des coups de pieds à l’âme, sinon au cul, de profiter de l’élan et de se dire que ça ira mieux, demain..."</em>
