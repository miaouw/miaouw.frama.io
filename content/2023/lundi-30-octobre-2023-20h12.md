+++
Author = "Mathul"
title = "Lundi 30 Octobre 2023 20h12"
date = 2023-10-30T20:12:21+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Elle voulait que tout s’allume. Elle aimait le solaire. Je préférais la lune...  
Tous les soirs elle s’inventait de petites prières pour qu’en elle toutes les couleurs se lavent de la nuit et refleurissent dans la lumière du matin...  
C’était une petite fille sans peluche, l’ogre les avait dévorés. Une petite fille qui ne traînait pas son enfance au bout d’une ficelle. L’ogre avait dévoré l’enfance et la ficelle... Elle disait que si elle s’envolait un jour, elle ne laisserait que le meilleur d’elle-même. Que je pourrais mettre ma main dans la sienne et regarder dehors...  
Elle avait un prénom de tempête...

