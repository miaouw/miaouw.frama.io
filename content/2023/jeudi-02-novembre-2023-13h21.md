+++
Author = "Mathul"
title = "Jeudi 02 Novembre 2023 13h21"
date = 2023-11-02T13:21:42+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
J’ai des rêves d’ermite. Ça remonte à l’enfance.  
Je voudrais mon chez-moi, à moi. Mon ermitage. Là-bas au moins le spleen laisse passer un peu de la couleur. Ici tout est pénible. Ça crie, ça geint, ça braille pour tout et surtout pour rien ! Ça essaie de se dégager… Ça fait mal ! Et l’ennui… terrible… ça vous parle un drôle de langage d’écorchés vifs. Lassitudes ! La vie se ratatine. Ni debout, ni couchée. Seulement trente-huit jours plus tard : déjà l’usure et le doute par-dessus...
