+++
Author = "Mathul"
title = "Vendredi 27 Octobre 2023 15h22"
date = 2023-10-27T15:22:22+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
<a href="/img/cigognegd.png" title=" " class="zoombox">
<span class="zoomomie"><img src="/img/cigognemin.png" alt=" " width="200" height="556" class="rightmo" loading="lazy" /></span></a><br />
Les premiers froids d’octobre sont là, et quelques gouttes de soleil avec...  
J’ai vu passer des troupeaux de grues. Alors je regarde passer... et le temps avec. Cafés et cigarettes.
Oh, que c’est assommant de passer son temps à attendre ! J’effeuille des livres, je profite de l’absence de pluie pour tourner en rond. Je voudrais bien dessiner un peu mais la main refuse. Pourtant j’ai tout un tas de chouettes crayons plein d’alcool. Le petit ukulélé dort dans son placard... Lui aussi refuse... Ça va revenir ! Demain c’est pleine lune et je suis peinard dans ma piaule. C’est bon dêtre peinard. Reste à savoir ce que je vais en foutre de ma tranquilité... hurler avec les loups ?!

Parfois j’imagine un bon Dieu.  
Un genre brave type sympathique qui a créé le monde mais qui très vite a été dépassé par les évènements. Il peut plus rien contre la guerre, la maladie, la dépression la mort et les méchantes choses qui font son monde. Pauvre vieux ! Et il faudrait l’aimer... parce qu’il doit être vachement malheureux. Tous les jours qu’il fait, ça doit pas le faire marrer de voir toute cette misère et de rien pouvoir faire pour y soulager... Je devrais peut-être lui suggérer de passer un peu de temps dans cet hôpital ou je tourne pas rond... Va savoir ! On regarderait passer les premières grues de l’automne... mais qu'il ne compte par sur moi pour croire en lui !!! Fichtre !{{< clear >}}<br /> <br />
