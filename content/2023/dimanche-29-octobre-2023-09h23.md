+++
Author = "Mathul"
title = "Dimanche 29 Octobre 2023 09h23"
date = 2023-10-29T09:23:41+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Aime-la cette vie. Bouleverse-toi d’elle. Pète-lui la gueule. Casses tout. Ça donne des ailes paraît... rêver ! Facile à dire ! Parfois il faut juste rendre ses rêves à l’évidence : tout s’en va en arabesques, en fuites. Il y a l’amer. Les moments maudits.  Un mot, une pensée et... flouf ! petit bonhomme file au néant ! Toute la soupière envahie soudain de vampires. L’esprit c’est plus qu’un sous-bois plein de monstres, faut pas somnoler... vermines qui rampent, qui rongent... tout dans les ombres, dans les ronces. L’ombre, c’est la seule lumière qui reste. Gluante, visqueuse, étouffante. C’est plus fort que toute existence. 
