+++
Author = "Mathul"
title = "Vendredi 03 Novembre 2023 20h30"
date = 2023-11-03T20:30:05+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Ils bouffent comme des vaches. Ils rêvent d’apéros. Ils discutent de conneries à perte de vue et surtout ils n’aiment pas se fatiguer. Un étage à monter, c’est l’ascenseur qui turbine ! Ils s’en foutent d’être moches, répugnants, mous et dégueulasses pourvu qu’ils aient plus de pilules à gober que le voisin. Avec vachement de couleur s’il vous plaît, et des effets secondaires pire que si c’était moins pire… Et puis ça suinte... Autant je puis comprendre qu’on revendique sa petite vérole, c’est vaguement viril… bon ! mais tes furoncles, tes glaires, tes hémorroïdes, ton ongle incarné et ta vieille viande puante… T’as déjà vu raconter ça à table ? Moi oui ! Et tous les jours encore !

Je jure, parfois il faut serrer les dents. C’est plus un hôpital, c’est un cinéma, grand Jacques ! Ils sont cons à se laisser crever sans rien faire contre. Sans même soupçonner qu’on pourrait essayer, du moment que les pilules et la soupe arrivent à l’heure… enfin !
