+++
Author = "mAdours"
title = "Jeudi 09 Novembre 2023 20h34"
date = 2023-11-09T20:34:57+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Ta main dans la mienne,  
On s'endort en mâchant  
Des herbes musiciennes  
Au milieu des chants  
D'un petit Tom tordu  
Et d'une Jerry têtue.  

La terre parle tout bas  
Des cœurs, des gens, du vent  
Et d'un tambour qui bat  
Plus fort que les tourments  
D'une petite luciole  
Et d'un chat de traviole.  

Ton âme, petite sœur,  
L'Arménie est dedans !  
Si les étoiles ont peurs  
Il y a dans ton chant  
Ta musique de souris  
Puis les câlins d'un ami...  

Mon petit ange, n'aie pas peur de l'orage !  
Même si la nuit pleut goutte à goutte  
C’est doux, c’est pas méchant,  
C’est que de l'amour qui coupe  
A travers champs...   

Du ciel sont tombées trois pommes :   
Une pour celui qui raconte,   
L’autre pour celui qui écrit   
Et la troisième pour celle qui lit.   
Que cela leur soit agréable...

