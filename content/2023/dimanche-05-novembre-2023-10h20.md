+++
Author = "mAdours"
title = "Dimanche 05 Novembre 2023 10h20"
date = 2023-11-05T10:20:04+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Mais c’est à mon sang qu’elles en veulent ces goules !  
Trente millilitres par jour depuis une semaine, même le dimanche... Il y a aiguille sous roche ! Ça doit se revendre à prix d’or... Me suis même amusé à lire le Vidal : c’est pas beau les petites pilules que je gobe matin, midi, soir, nuit. Pourtant j’aime bien les arcs-en-ciel, mais là... c’est de la viande morte qu’on cherche à faire tressaillir. Le doute s’installe sévèrement... ça occupe !
