+++
Author = "Mathul"
title = "Mardi 10 Octobre 2023 13h26"
date = 2023-10-10T13:26:51+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = true
+++


<a href="/img/mairmaid_01gd.png" title=" " class="zoombox">
<span class="zoomomie"><img src="/img/mairmaid_01min.png" alt=" " width="300" height="320" class="leftmo" /></span></a><br />
Je suis un vieux loup qui court après une charogne. Avec une jambe d’ivoire un œil de feu. Un de ceux qui harponnent les sirènes...  

Dans un verre, dans un lac, dans un océan tout entier. Je vais glanant des trucs dégueulasses au jusant de la vie. Je pioche des graines fauves, mauves et absinthes. Des trucs portés par le vent. Je sais des paradis sans frontières. Des rivages sans foi ni loi, sans feu ni eau, ni lieu. Des profondeurs où semer ce genre de choses. Des océans d’orage. Ils me manquent ! La vie c’est toujours la mer à boire. On y navigue seulement aux nuages… Même les étoiles, ça se bouffe !{{< clear >}}

