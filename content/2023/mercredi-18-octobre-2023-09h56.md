+++
Author = "Mathul"
title = "Mercredi 18 Octobre 2023 09h56"
date = 2023-10-18T09:56:33+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Pffft ! J’aurais bien voulu une maladie qui me fasse au moins œuvrer. Un peu écrire. Un peu rêver. Mais elle m’anéantit... je suis tout blet et plat. A remâcher sans cesse... ressasseur... En croix. Même miné au physique. Tout en hésitations. Sans desseins. Sans envies aucunes...  

Ce matin encore, vivisecté à l’aiguille. Prenez donc tout ! Pompez, pompez ! Même mes petits chapelets de viscères, mes dentelles d’organes. Prenez, prenez ! Parfois l’envie de hurler... Bordel ! Arrêter vos saintes pilules miraculeuses. Laissez-venir les petites lumières du fond des corridors. Découpez-moi la cervelle ! Bocal ! Formol pour les collectionneurs. Gardez-les tous mes capharnaüms... !  
Qu’on n’en parle plus ! A moins d’un dernier baiser pour la route...

