+++
Author = "Mathul"
title = "Jeudi 12 Octobre 2023 11h11"
date = 2023-10-12T11:11:13+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Sans elle, un instant, vous existez plus !  
L’éthanol, ça rendait plus bavard. C’était pour mieux fermer la gueule au monstre caché sous le lit. A celui qui t’as aspiré toute ta fontanelle. A celui qui a égorgé toute ta volonté.  

L’ivresse... petite fée verte effrénée, bagareuse, à tenter toujours de bâillonner la bête. Mais enfin...  l'ivresse ! Pour l’exaltation. Pour oublier les freins. Pour croire, dire, ou entendre des mots plus doux, plus enthousiastes sur le vécu... Le vrai, le beau, le faux, le laid. Pour avouer, peut-être enfin, qu’on cache au fond de sa poche une sylphide armée d’une pioche... mais qu’elle est bien incapable !  

Foutre ! Mais t’es tout seul mon monstre à être ton monstre !!!  
Ta fée c’est toi qui en fait une connasse !  

Ta fée c’est une luciole ! Et les lucioles ça ne traîne pas trop dans les tunnels...
