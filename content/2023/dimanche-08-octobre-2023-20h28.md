+++
Author = "Mathul"
title = "Dimanche 08 Octobre 2023 20h28"
date = 2023-10-08T20:28:51+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Etre là, chacune des secondes… Essayer sans s’abandonner totalement. Se revenir vivant. Essayer de tout prendre à la fois. Tous les bruits de la nuit, toutes les ombres, les échos qui glissent, l’arôme du venin et celui des baisers. Essayer de ne pas sombrer à chaque battement. Obsession ! Pute d’angoisse. Vieille maquerelle qui danse. Serpent à plume et lanterne rouge… tu tapines, tu t'immisces, et sans fards encore. Inutiles ! Mais je vais t’avoir à l’usure, petite salope ! Essayer l’entier, être moi !

J’ai déjà traversé bien des agonies… ici… là-bas… partout ! De belles, de discrètes de trop fidèles... Je connais des forêts d’où l’on ne revient jamais. Jamais tout à fait… et tout un tralala de maléfices. Mais… tout prendre à la fois ! Je connais le truc : il n’y a pas de surprise aux excès. Etre là, chacune des secondes...
