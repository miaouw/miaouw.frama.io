+++
Author = "mAdours"
title = "Mardi 14 Novembre 2023 17h14"
date = 2023-11-14T17:14:22+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
Retiré du monde courant... Condamné à la rêvasserie... Pesé, piqué, numéroté, sapé en nuage et tutoyé... La taule, ou pas loin ! Je voulais mettre des mots plus doux sur le vécu. Travailler à colorier l’horizon. Je voulais repartir de zéro, rattaquer la vie d’un autre sens... plus à l’enthousiasme, à l’élan, acharné, attentif. Maintenant je suis hésitant sur tout.

J’éponge la vie au fur à mesure qu’elle s’écoule, seconde par seconde. C’est qu’elle a du caractère, elle demande à s’évader. Sortir à flot. Comme on crève une artère. Pas de vivre à regret, à tout petits coups... Des mots plus doux... tu parles ! La lumière de dix-sept heures, qui ne dit rien, mais qui fout l’âme à l’envers. On s’habituerait... presque, hélas ! Mais très mal !
