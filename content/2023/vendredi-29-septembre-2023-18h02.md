+++
Author = "Mathul"
title = "Vendredi 29 septembre 2023 18h02"
date = 2023-09-29T18:02:59+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++

Je marche, je tourne, je cours et, plus je cours, moins je trouve d'endroit pour me cacher.  
Je n'arrive plus à parler, je ne sais plus me taire. A se demander si cette fois je reviendrai dedans la vie... 

