+++
Author = "mAdours"
title = "Vendredi 10 Novembre 2023 20h22"
date = 2023-11-10T20:22:03+01:00
Categories = ["2023"]
menu = "main"
draft = false
art = true
+++

<a href="/img/scaphgd.png" title=" " class="zoombox">
<span class="zoomomie"><img src="/img/scaphmin.png" alt="." width="200" height="224" class="rightmo" loading="lazy" /></span></a>
<br /><br />Pénible de passer son temps à se rafistoler l’âme !  
Se persuader de sa force et, dès le lendemain, lâcher prise. Défaillir, dégringoler de partout. La lente et longue chute... Celle qui donne la peur du vide en soi. De la partie équivoque, mystérieuse, inconnue. De celle qui brise. Celle qui fracasse toutes certitudes...  
Et chaque jour encore... croire !
{{< clear >}}



