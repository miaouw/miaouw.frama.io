+++
Author = "Mathul"
title = "Lundi 25 septembre 2023 03h03"
date = 2023-09-25T03:03:03+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = false
+++
La promesse est une petite chose bien salope !  
Chienne folle ! Indomptable si tu fais pas gaffe... Elle arrive toujours avec son petit orchestre de : <em>“demain, plus tard, on verra, peut-être, selon la partition, le sens du vent, les couleurs du paysage, etc, etc...”</em>  

La vie, c’est des mouvements, des soupirs... Alors les promesses, tu parles ! Fantômes ! Un courant d’air ça te les balance sur orbite ! Je me demande où sont passées mes colères : certainement qu’elles habitent avec mes promesses, dans le même labyrinthe que mes trouilles et mes désespoirs. Étrange collocation !  

Mais hier, j’ai promis... Hôpital psychiatrique... Sauver ce qu’il reste de viande, ramasser les copeaux d’âme. Descendre, la trouille au ventre, dans le méchant gouffre qui griffe, qui tout maquille et croque. Aujourd’hui, j’y suis ! Et dans l’éther encore... Mais ma petite promesse... ma luciole ! Je vais revenir des outres-mondes.  

L’astuce, quand on a promis, c’est de tenir tout de suite ! Maintenant ! Même dans l’effroi ! Même si toujours se mêle au milieu de ce qu’on veut faire et soi-même, milles mignonnes petites choses bien gentilles, bien lâchent et bien empêcheuses. C’est dingue comme être soi est un travail solitaire...  
