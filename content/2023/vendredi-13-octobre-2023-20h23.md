+++
Author = "Mathul"
title = "Vendredi 13 Octobre 2023 20h23"
date = 2023-10-13T20:23:20+02:00
Categories = ["2023"]
menu = "main"
draft = false
art = true
+++
Traîner tête basse dans la lumière des néons. Ne plus savoir ce qu’il reste du jour ou de la nuit. Il fallait y être bien sûr... la promesse ! Je connais, j’ai vu déjà, ailleurs... autrement. J’ai cru que c’était il y a longtemps, dans le jadis...  
Mais ce soir, ça fatigue, ça remonte. Mémoire salope ! 
<a href="/img/mairmaid_02gd.png" title=" " class="zoombox">
<span class="zoomomie"><img src="/img/mairmaid_02min.png" alt=" " width="209" height="300" class="rightmo" /></span></a>
 

Rien n’est vraiment fine fleur ! A genoux, couché, à la bouffe, aux chiottes, à la douche. C’est attristant comme s’occupe une journée. Les imaginations qu’il faut pour assassiner une pendule. Dragées arc-en-ciel pour tout repas. Sortir entre trente clopes et vingt cafés immondes pour tasser le tout. Essayer parfois d’être un peu au monde... sortir, marcher à petit pas. Rêver laudanum. Croiser une ambulance. A quand la mienne ? La mienne à moi ?! Errances... méandres, détours, seul et sans qu’une putain de lumière vienne porter secours. Même pas ces putasses de pilules que tout assomme lâchement ! Tout s’évapore dans la caboche. A commencer par les mots...

Reste le doute. Les hésitations. Les vieilles lunes... on ne voile pas les vieilles lunes.  
Il y a mes Paulo pourtant... ils y croient eux, je les sais... !{{< clear >}}
